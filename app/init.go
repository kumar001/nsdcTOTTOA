package app

import (
	"io/ioutil"
	"net/http"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"time"

	"github.com/gorilla/mux"

	"github.com/sirupsen/logrus"

	"github.com/gorilla/context"
)

// Init
// func Init(logger *logrus.Logger) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		now := time.Now()

// 		rc.Response = &access.LogResponseWriter{rc.Response, http.StatusOK, 0}

// 		ac := newRequestScope(now, logger, rc.Request)
// 		rc.Set("Context", ac)

// 		fault.Recovery(ac.Errorf, convertError)(rc)
// 		logAccess(rc, ac.Infof, ac.Now())
// 	})
// }

// Init :
func Init(logger *logrus.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			now := time.Now()
			ac := newRequestScope(now, logger, r)

			ac.SetDB()
			defer ac.DB().Session.Close()

			b, _ := ioutil.ReadAll(r.Body)
			ac.SetBody(b)
			defer r.Body.Close()

			ac.SetParams(mux.Vars(r))

			ac.SetQueries(r.URL.Query())

			context.Set(r, "Context", ac)
			defer context.Clear(r)

			// Read JWTClaims fro the context
			c := context.Get(r, constant.CONTEXTJWTKEY).(models.JWTClaims)
			ac.SetJWTClaims(&c)

			next.ServeHTTP(w, r)
			// TODO: Print access log here
			return
		})
	}
}

// GetRequestScope returns the RequestScope of the current request.
func GetRequestScope(r *http.Request) RequestScope {
	if val := context.Get(r, "Context"); val != nil {
		return context.Get(r, "Context").(RequestScope)
	} else {
		return new(requestScope)
	}
}
