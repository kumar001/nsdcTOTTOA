package middleware

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"nsdctottoaservice/models"
	"userservice/constant"
	//"totservice/logger"

	"github.com/gorilla/context"
)

type utils interface {
	HTTPGet(url string, h map[string]string) (*http.Response, error)
}

// Middleware :
type Middleware struct {
	utils utils
}

// GetMiddlewares :
func GetMiddlewares(u utils) *Middleware {
	return &Middleware{u}
}

// Adapter :
type Adapter func(http.Handler) http.Handler

// AllowCors :
func (m *Middleware) AllowCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")

		if r.Method == "OPTIONS" {
			return
		}
		next.ServeHTTP(w, r)
		return
	})
}

// HasLoggedIn : this function checks the validity of the access token
func (m *Middleware) HasLoggedIn(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// #1 Check Access token,
		token := r.Header.Get("Authorization")
		if token == "" {
			w.WriteHeader(http.StatusPreconditionFailed)
			fmt.Fprintf(w, "Unauthorized")
			return
		}
		h := make(map[string]string)
		h["Authorization"] = token

		// TODO: add auth service url into a config file
		resp, err := m.utils.HTTPGet(constant.AUTHSERVICE+"/auth", h)
		if err != nil {
			w.WriteHeader(http.StatusPreconditionFailed)
			fmt.Fprintf(w, "Unauthorized")
			return
		}

		context.Set(r, "loggedIn", true)

		jwtC := &models.JWTClaims{}

		// var xx map[string]interface{}

		if err := json.NewDecoder(resp.Body).Decode(&jwtC); err != nil || jwtC.UserName == "" {
			w.WriteHeader(http.StatusPreconditionFailed)
			fmt.Fprintf(w, "Unauthorized")
			return
		}
		context.Set(r, constant.CONTEXTJWTKEY, *jwtC)
		next.ServeHTTP(w, r)
	})
}

// LoggingMiddleware ... Loggin the  Api end POint @params {http Request}
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Log @ {request URL}
		log.Println(r.RequestURI, r.Method)

		// Call the next handle
		next.ServeHTTP(w, r)
	})
}
