# NoSchemeTrainingCenter

> this service will create batches(tot, toa) 

### Steps to run
- change the working dir, go noschemetrainingcentere dir
- ~~then run `dep ensure` to install all dependencies~~
    </br>~~NOTE: if you don't have `dep` in your system then [check here](https://github.com/golang/dep)~~</br>
    **Use `go get ./...` to install dependencies**
    
- Run mongoDB
- Run command `go run main.go` or `go build`


-----

# Test

*Still under progress*

- run `go test ./... [-cover]` 
