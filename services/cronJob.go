package services

import (
	"nsdctottoaservice/app"

	"gopkg.in/mgo.v2/bson"
)

type cronJobDAO interface {
	UpDateBatches(rs app.RequestScope, findQuery, updateQuery bson.M) error
	GetBatchIdsFromBwflow(rs app.RequestScope, findQuery bson.M) (batchIds []int64, err error)
	UpDateBatchworkFlows(rs app.RequestScope, findQuery, updateQuery bson.M) error
}
type cronJobService struct {
	dao cronJobDAO
}

// NewCronJobServices...
func NewCronJobServices(dao cronJobDAO) *cronJobService {
	return &cronJobService{dao}
}

// AutoRejectBatch ... Auto Reject a Batch For A Particular User role
func (s *cronJobService) UpDateBatches(rs app.RequestScope, findQuery, updateQuery bson.M) error {
	return s.dao.UpDateBatches(rs, findQuery, updateQuery)
}

func (s *cronJobService) GetBatchIdsFromBwflow(rs app.RequestScope, findQuery bson.M) ([]int64, error) {
	return s.dao.GetBatchIdsFromBwflow(rs, findQuery)
}

func (s *cronJobService) UpDateBatchworkFlows(rs app.RequestScope, findQuery, updateQuery bson.M) error {
	return s.dao.UpDateBatchworkFlows(rs, findQuery, updateQuery)
}
