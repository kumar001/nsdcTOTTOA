package services

import (
	"errors"
	//"testing"

	//"nsdctottoaservice/utils"
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"

	"gopkg.in/mgo.v2/bson"
)

type IFakeDAO interface {
	CreateBatch(app.RequestScope, *models.Batch) error
	AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error
	GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error)
	// GetAppliedCandidates(app.RequestScope, string) (*models.Batch, error)
	GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) (*models.Batch, error)
	GetBatch(rs app.RequestScope, query bson.M, peoject bson.M) (*models.Batch, error)
	UpdateBatchByID(rs app.RequestScope, batchID int64, update interface{}) error
	UpdateBatchWithFindQuery(rs app.RequestScope, query, update interface{}) error
	FindOneBatch(rs app.RequestScope) (b *models.Batch, err error)
	GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchcount int, err error)
	FindBatchTypes(rs app.RequestScope) (b []*models.BatchType, err error)
}

type FakeDAO struct {
}

type FakeErrorDAO struct {
}

func (f *FakeDAO) CreateBatch(app.RequestScope, *models.Batch) error {
	return nil
}

func (f *FakeErrorDAO) CreateBatch(app.RequestScope, *models.Batch) error {
	return errors.New("Error in DB")
}

func (f *FakeDAO) FindBatchTypes(app.RequestScope) ([]*models.BatchType, error) {
	fakeBatchTypeData := CreateFakeBatchTypesData()
	return fakeBatchTypeData, nil
}

func (f *FakeErrorDAO) FindBatchTypes(app.RequestScope) ([]*models.BatchType, error) {
	return nil, errors.New("Error in DB")
}

func (f *FakeDAO) AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error {
	return nil
}

func (f *FakeErrorDAO) AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error {
	return errors.New("Error in DB")
}

func (f *FakeDAO) GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error) {
	cd := CreateCandidateModel()
	return cd, nil
}

func (f *FakeErrorDAO) GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error) {
	return nil, errors.New("Error in DB")
}

func (f *FakeDAO) FindOneBatch(app.RequestScope) (*models.Batch, error) {
	fakeBatchData := CreateFakeBatchData()
	return fakeBatchData, nil
}

func (f *FakeErrorDAO) FindOneBatch(app.RequestScope) (*models.Batch, error) {
	return nil, errors.New("Error in DB")
}

func (f *FakeDAO) GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) (*models.Batch, error) {
	fakeBatchData := CreateFakeBatchData()
	return fakeBatchData, nil
}

func (f *FakeErrorDAO) GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) (*models.Batch, error) {
	return nil, errors.New("Error in DB")
}

//WARNING : DO NOT REMOVE COMMENTED CODE

// func TestCreateBatchSvc(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	fakeBatchData := CreateFakeBatchData()
// 	// r := new(requestScope)
// 	r := new(http.Request)

// 	rr := app.GetRequestScope(r)
// 	rr.SetDB()
// 	jwt := models.JWTClaims{Role: "SSC", UserName: "SSC_USER"}
// 	rr.SetJWTClaims(&jwt)
// 	rr.Now()
// 	err := b.CreateBatchSvc(rr, fakeBatchData)
// 	if err != nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestCreateBatchSvcWithDBError(t *testing.T) {
// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	fakeBatchData := CreateFakeBatchData()
// 	// r := new(requestScope)
// 	r := new(http.Request)

// 	rr := app.GetRequestScope(r)
// 	rr.SetDB()
// 	jwt := models.JWTClaims{Role: "SSC", UserName: "SSC_USER"}
// 	rr.SetJWTClaims(&jwt)
// 	rr.Now()
// 	err := b.CreateBatchSvc(rr, fakeBatchData)
// 	if err == nil {
// 		t.Error("Expected error not to be nil, Got result")
// 	}
// }

// func TestGetBatchTypes(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	_, err := b.GetBatchTypes(rr)
// 	if err != nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestGetBatchTypesWithDBError(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	_, err := b.GetBatchTypes(rr)
// 	if err == nil {
// 		t.Error("Expected error not to be nil, got the result")
// 	}
// }

// func TestAddCandidateToBatch(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	fakeCandidateData := CreateFakeCandidateData()
// 	err := b.AddCandidateToBatch(rr, bid, fakeCandidateData)
// 	if err != nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestAddCandidateToBatchWithDBError(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	fakeCandidateData := CreateFakeCandidateData()
// 	err := b.AddCandidateToBatch(rr, bid, fakeCandidateData)
// 	if err == nil {
// 		t.Error("Expected error not to be nil, got the result")
// 	}
// }

// func TestGetCandidatesBasedOnStatus(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	err := b.GetCandidatesBasedOnStatus(rr, bid, "Applied")
// 	if err != nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestGetCandidatesBasedOnStatusWithDBError(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	err := b.GetCandidatesBasedOnStatus(rr, bid, "Applied")
// 	if err == nil {
// 		t.Error("Expected error not to be nil, got the result")
// 	}
// }

// func TestGetOneBatch(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	batch,err := b.GetOneBatch(rr)
// 	if err != nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestGetOneBatchWithDBError(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	batch, err := b.GetOneBatch(rr)
// 	if err == nil {
// 		t.Error("Expected error not to be nil, got the result")
// 	}
// }

// func TestGetOneBatch(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	projection := bson.M{}
// 	projection["Size"] = 1
// 	projection["Status"] = "Created"
// 	projection["BatchName"] = "BATCH001"

// 	batch:= b.GetBatchByID(rr, bid,projection)
// 	if batch == nil {
// 		t.Error("Expected result, Got error")
// 	}
// }

// func TestGetOneBatchWithDBError(t *testing.T) {

// 	ut := utils.GetUtils()

// 	b := BatchService{dao: new(FakeErrorDAO), utils: ut}
// 	r := new(http.Request)
// 	rr := app.GetRequestScope(r)
// 	bid := int64(2)
// 	projection := bson.M{}
// 	projection["Size"] = 1
// 	projection["Status"] = "Created"
// 	projection["BatchName"] = "BATCH001"

// 	batch:= b.GetBatchByID(rr, bid,projection)
// 	if batch != nil {
// 		t.Error("Expected error not to be nil, got the result")
// 	}
// }

func CreateFakeBatchData() *models.Batch {
	m := models.Batch{}
	m.Size = 30
	m.Hours = 5
	return &m
}

func CreateFakeBatchTypesData() []*models.BatchType {

	var result []*models.BatchType
	m := models.BatchType{}
	m.ID = "20"
	m.Name = "Some_Batch_Name"
	result = append(result, &m)
	return result
}

func CreateFakeCandidateData() *models.Candidate {
	m := models.Candidate{}
	m.Name = "XYZ"
	m.Email = "candidate@email"
	m.Phone = 9999999999
	m.UserName = "cand_01"
	m.UserRole = "Trainer"
	return &m
}

func CreateCandidateModel() []bson.M {
	var mod []bson.M
	m := bson.M{}
	m["userName"] = "userName"
	m["email"] = "email@email"
	m["userRole"] = "userRole"
	mod = append(mod, m)
	return mod
}
