package services

import (
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"
	"testing"
)

type fakeAdminDao struct {
	dao adminDAO
}

func NewFakeAdminDao(dao adminDAO) *fakeAdminDao {
	return &fakeAdminDao{dao}
}

func (f fakeAdminDao) GetAdminConfiguration(app.RequestScope, []string) (models.TOTConfiguration, error) {
	return models.TOTConfiguration{}, nil
}

func (f fakeAdminDao) GetAdminConfigurationToa(app.RequestScope, []string) (models.TOAConfiguration, error) {
	return models.TOAConfiguration{}, nil
}

func TestGetAdminConfiguration(t *testing.T) {

	//adminservice := new(fakeAdminDao)

	//	adminservice.GetAdminConfiguration()

}
