package services

import (
	"fmt"
	"io"
	"smartservice/shared"
)

type UsersServiceDAO interface {
	GetUserBasedOnUserName(username string, role string, header map[string]string) (io.ReadCloser, error)
}

type UsersService struct {
	UsersServiceDAO UsersServiceDAO
}

func (usersService *UsersService) GetUserBasedOnUserName(username string, role string, header map[string]string) (io.ReadCloser, error) {
	url := "http://localhost:3000/v1/user/" + role + "/username/" + username
	fmt.Println(url, "URL")
	resp, err := shared.Get(url, header)
	if err != nil {
		return nil, err
	}
	return resp.Body, err
}
