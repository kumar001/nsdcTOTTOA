package services

import (
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/daos"
	"nsdctottoaservice/models"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

type utilsBW interface {
	NewError(status int, message interface{}) *models.Error
	NextCount(rs app.RequestScope, key string) int64
}

type IbatchworkflowSvc interface {
	Post(app.RequestScope, *models.BatchWorkflow) *models.Error
	CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) *models.Error
}

type IbatchworkflowService interface {
	GetBatchWorkflow(r app.RequestScope, v map[string]string) ([]*models.BatchWorkflow, error)
	UpdateBatchWorkflow(rs app.RequestScope, reqID int64, patch bson.M) *models.Error
	GetOne(rs app.RequestScope, query, project bson.M) (*models.BatchWorkflow, *models.Error)
	GetPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) ([]bson.M, int, error)
	GetPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error)
}

// type batchWorkflowDAO interface {
// 	FindPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) (bwdata []bson.M, pendingBatchesCount int, err error)
// 	Post(app.RequestScope, *models.BatchWorkflow) error
// 	FindBatchworkflow(rs app.RequestScope, userDetails map[string]string) ([]*models.BatchWorkflow, error)
// 	CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) error

// 	GetOne(rs app.RequestScope, query, project bson.M) (bw *models.BatchWorkflow, err error)
// 	Update(rs app.RequestScope, query bson.M, bw *models.BatchWorkflow) error
// 	FindPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error)
// }

// BatchWorkflowService :
type BatchWorkflowService struct {
	dao   daos.IbatchWorkflowDAO
	utils utilsBW
}

// NewBatchWorkflowService :
func NewBatchWorkflowService(dao daos.IbatchWorkflowDAO, utils utilsBW) *BatchWorkflowService {
	return &BatchWorkflowService{dao, utils}
}

// Post : service
func (bws *BatchWorkflowService) Post(rs app.RequestScope, bw *models.BatchWorkflow) *models.Error {
	bw.ReqID = bws.utils.NextCount(rs, models.CollectionBatchWorkflow)
	err := bws.dao.Post(rs, bw)
	if err != nil {
		return bws.utils.NewError(http.StatusInternalServerError, err)
	}
	return nil
}

// GetBatchWorkflow : service
func (bws *BatchWorkflowService) GetBatchWorkflow(rs app.RequestScope, userDetails map[string]string) ([]*models.BatchWorkflow, error) {
	return bws.dao.FindBatchworkflow(rs, userDetails)
}

// CancelPendingWorkflow :  service
func (bws *BatchWorkflowService) CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) *models.Error {
	err := bws.dao.CancelPendingWorkflow(rs, batchID, qpCode, toUserRole, userName)
	if err != nil {
		return bws.utils.NewError(http.StatusInternalServerError, err)
	}
	return nil
}

// GetPendingBatches :
func (bws *BatchWorkflowService) GetPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) ([]bson.M, int, error) {

	return bws.dao.FindPendingBatches(rs, tokenValue, paginate)
}

// GetOne :
func (bws *BatchWorkflowService) GetOne(rs app.RequestScope, query, project bson.M) (*models.BatchWorkflow, *models.Error) {
	var bw *models.BatchWorkflow
	var err error
	if bw, err = bws.dao.GetOne(rs, query, project); err != nil {
		if err.Error() == "not found" {
			return bw, bws.utils.NewError(http.StatusNotFound, "Invalid ID")
		}
		return bw, bws.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}
	return bw, nil
}

// UpdateBatchWorkflow :
func (bws *BatchWorkflowService) UpdateBatchWorkflow(rs app.RequestScope, reqID int64, patch bson.M) *models.Error {
	q := bson.M{"reqId": reqID}
	sel := bson.M{}
	bw, err := bws.dao.GetOne(rs, q, sel)

	if err != nil {
		if err.Error() == "not found" {
			return bws.utils.NewError(http.StatusNotFound, "Invalid ID")
		}
		return bws.utils.NewError(http.StatusInternalServerError, err)
	}

	// got BW
	bw.ActionTakenOn = rs.Now()
	if patch["status"] != nil {
		status := strings.ToUpper(patch["status"].(string))
		bw.Status = status
	}
	if patch["comment"] != nil {
		bw.Comment = patch["comment"].(string)
	}

	// TODO: assign all other prop value to struct, for now expecting to values

	if err := bws.dao.Update(rs, q, bw); err != nil {
		return bws.utils.NewError(http.StatusInternalServerError, err)
	}
	return nil
}

// GetPendingBatch :
func (bws *BatchWorkflowService) GetPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error) {
	return bws.dao.FindPendingBatch(rs)
}
