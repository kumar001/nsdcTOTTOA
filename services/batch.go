package services

import (
	"errors"
	"fmt"
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/daos"
	"nsdctottoaservice/models"

	"strconv"

	"gopkg.in/mgo.v2/bson"
)

type IbatchService interface {
	CreateBatchSvc(app.RequestScope, *models.Batch) error
	AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error
	GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error)
	// GetAppliedCandidates(app.RequestScope, string) (*models.Batch, error)
	UpdateMTForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
	UpdateAAForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
	UpdateAssessorForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
	GetBatchBasedOnCandidatesStatus(rs app.RequestScope) ([]*models.Batch, int, error)

	UpdateBatchWithFindQuery(rs app.RequestScope, query, patch interface{}) *models.Error

	GetBatch(app.RequestScope, bson.M, bson.M) *models.Batch
	GetOneBatch(rs app.RequestScope) (b *models.Batch, err error)
	GetBatchTypes(rs app.RequestScope) ([]*models.BatchType, error)
	// GetBatch(rs app.RequestScope, batchID int64) *models.Batch
	GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchcount int, err error)
	CancelBatch(rs app.RequestScope) error
}

type IbatchSvcInBW interface {
	GetBatch(rs app.RequestScope, query bson.M, project bson.M) *models.Batch
	UpdateBatchByID(rs app.RequestScope, batchID int64, data interface{}) *models.Error
}

type iutils interface {
	JSONStrToBsonM(string) bson.M
	NextCount(app.RequestScope, string) int64
	NewError(status int, message interface{}) *models.Error
}

// BatchService :
type BatchService struct {
	dao   daos.IbatchDAO
	utils iutils
}

// NewBatchService : creates instance of batch service
func NewBatchService(dao daos.IbatchDAO, u iutils) *BatchService {
	return &BatchService{dao, u}
}

// CreateBatchSvc :
func (bs *BatchService) CreateBatchSvc(rs app.RequestScope, batch *models.Batch) error {
	batch.BatchID = bs.utils.NextCount(rs, models.CollectionBatch)
	batch.CreatedOn = rs.Now()
	// TODO: move all const to constant package
	batch.Status = "BATCH CREATED"

	// TODO: Check if the actor with UserName is a SSC or not
	if rs.GetJWTClaims().Role != "SSC" {
		rs.Warn("Expected SSC Role, but got ", rs.GetJWTClaims().Role)
		return errors.New("Unauthorized")
	}
	batch.CreatedBy = rs.GetJWTClaims().UserName
	return bs.dao.CreateBatch(rs, batch)
}

// GetBatchTypes :
func (bs *BatchService) GetBatchTypes(rs app.RequestScope) ([]*models.BatchType, error) {
	return bs.dao.FindBatchTypes(rs)
}

// AddCandidateToBatch :
func (bs *BatchService) AddCandidateToBatch(rs app.RequestScope, batchID int64, candidate *models.Candidate) error {
	return bs.dao.AddCandidateToBatch(rs, batchID, candidate)
}

// GetCandidatesBasedOnStatus :
func (bs *BatchService) GetCandidatesBasedOnStatus(rs app.RequestScope, batchID int64, status string) ([]bson.M, error) {
	return bs.dao.GetCandidatesBasedOnStatus(rs, batchID, status)
}

// UpdateMTForJobRole :
func (bs *BatchService) UpdateMTForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error {

	batch := &models.Batch{}
	var err error

	// json strings
	qs := fmt.Sprintf(`{"batchId":%d, "jobRoles.qpCode":"%s"}`, batchID, qpCode)
	ps := `{"jobRoles": 1, "batchId": 1, "tcApproved": 1}`

	// bson.M
	qB := bs.utils.JSONStrToBsonM(qs)
	pB := bs.utils.JSONStrToBsonM(ps)

	// #1 Get the batch and project the jobID
	batch, err = bs.dao.GetBatch(rs, qB, pB)
	if err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	if batch.TCApproved != constant.ACCEPTED {
		rs.Warn("TC Rejected Btach")
		return bs.utils.NewError(http.StatusBadRequest, "Cannot assign MT, Batch is pending for approval from TC")
	}

	var jobRoleIndex = -1
	// Check if the qpCode exists in the jobRoles
	for i, val := range batch.JobRoles {
		if val.QpCode == qpCode {
			jobRoleIndex = i
			break
		}
	}

	// FIXME: before updating, check if the role is ssc and userName in JWT and userName batch doc are same

	// #2 Check if it is hitting the assignedCountLimit, if yes then return Forbidden with some error
	if batch.JobRoles[jobRoleIndex].MasterTrainerDetails.AssignedCount >= constant.MTMAXASSIGNCOUNT {
		// hits the assign count limit
		return bs.utils.NewError(http.StatusForbidden, "Cannot Assign Master Trainer")
	}

	// #3 Update the batch according to the patch data
	// #3.1 Check if the MT has accepted the request, if yes? then restrict the the update
	if batch.JobRoles[jobRoleIndex].MasterTrainerDetails.Status == constant.ACCEPTED {
		// MT has already accepted the request
		return bs.utils.NewError(http.StatusForbidden, "Master Trainer has already accepted the request")
	}

	batch.JobRoles[jobRoleIndex].MasterTrainerDetails.Name = patch["name"].(string)
	batch.JobRoles[jobRoleIndex].MasterTrainerDetails.UserName = patch["userName"].(string)
	batch.JobRoles[jobRoleIndex].MasterTrainerDetails.Status = constant.PENDING

	// #4 Increment the assignedCount by 1
	batch.JobRoles[jobRoleIndex].MasterTrainerDetails.AssignedCount++

	// #5 Save the batch
	if err := bs.dao.UpdateBatchByID(rs, batchID, batch); err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	// #6 Cancel pending notification of MasterTrainer if exists
	rs.Info("Cancel all pending request to MT")

	// #7 Notify MasterTrainer
	rs.Info("notify MT")

	// INFO: step #6 and #7, written inside the batch apis

	return nil
}

// UpdateAAForJobRole :
func (bs *BatchService) UpdateAAForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error {
	batch := &models.Batch{}
	var err error

	// json strings
	qs := fmt.Sprintf(`{"batchId":%d, "jobRoles.qpCode":"%s"}`, batchID, qpCode)
	ps := `{"jobRoles": 1, "batchId": 1, "tcApproved": 1}`

	// bson.M
	qB := bs.utils.JSONStrToBsonM(qs)
	pB := bs.utils.JSONStrToBsonM(ps)

	// #1 Get the batch and project the jobID
	batch, err = bs.dao.GetBatch(rs, qB, pB)
	if err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	if batch.TCApproved != constant.ACCEPTED {
		rs.Warn("TC Rejected Btach")
		return bs.utils.NewError(http.StatusBadRequest, "Cannot assign Assesment Agency, Batch is pending for approval from TC")
	}

	var jobRoleIndex = -1
	// Check if the qpCode exists in the jobRoles
	for i, val := range batch.JobRoles {
		if val.QpCode == qpCode {
			jobRoleIndex = i
			break
		}
	}

	// FIXME: before updating, check if the role is ssc and userName in JWT and userName batch doc are same

	// #2 Check if it is hitting the assignedCountLimit, if yes then return Forbidden with some error
	if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssignedCount >= constant.AAMAXASSIGNCOUNT {
		// hits the assign count limit
		return bs.utils.NewError(http.StatusForbidden, "Cannot Assign Assessment Agency")
	}

	// #3 Update the batch according to the patch data
	// #3.1 Check if the AA has accepted the request, if yes? then restrict the the update
	if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.Status == constant.ACCEPTED {
		// AA has already accepted the request
		return bs.utils.NewError(http.StatusForbidden, "Assessment Agency has already accepted the request")
	}

	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.Name = patch["name"].(string)
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.UserName = patch["userName"].(string)
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.Status = constant.PENDING

	// #4 Increment the assignedCount by 1
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssignedCount++

	// #5 Save the batch
	if err := bs.dao.UpdateBatchByID(rs, batchID, batch); err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	// #6 Cancel pending notification of AA if exists
	rs.Info("Cancel all pending request to AA")

	// #7 Notify AA
	rs.Info("notify AA")

	// INFO: step #6 and #7, written inside the batch apis

	return nil
}

// UpdateAssessorForJobRole :
func (bs *BatchService) UpdateAssessorForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error {

	batch := &models.Batch{}
	var err error

	// json strings
	qs := fmt.Sprintf(`{"batchId":%d, "jobRoles.qpCode":"%s"}`, batchID, qpCode)
	ps := `{"jobRoles": 1, "batchId": 1, "tcApproved": 1}`

	// bson.M
	qB := bs.utils.JSONStrToBsonM(qs)
	pB := bs.utils.JSONStrToBsonM(ps)

	// #1 Get the batch and project the jobID
	batch, err = bs.dao.GetBatch(rs, qB, pB)
	if err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	if batch.TCApproved != constant.ACCEPTED {
		rs.Warn("TC Rejected Btach")
		return bs.utils.NewError(http.StatusBadRequest, "Cannot assign MT, Batch is pending for approval from TC")
	}

	var jobRoleIndex = -1
	// Check if the qpCode exists in the jobRoles
	for i, val := range batch.JobRoles {
		if val.QpCode == qpCode {
			jobRoleIndex = i
			break
		}
	}

	// FIXME: before updating, check if the role is "Assessment Agency" and userName in JWT and assigned userName are same

	// #2 Check if it is hitting the assignedCountLimit, if yes then return Forbidden with some error
	if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.AssignedCount >= constant.AMAXASSIGNCOUNT {
		// hits the assign count limit
		return bs.utils.NewError(http.StatusForbidden, "Cannot Assign Assessor")
	}

	// #3 Update the batch according to the patch data
	// #3.1 Check if the Assessor has accepted the request, if yes? then restrict the the update
	if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.Status == constant.ACCEPTED {
		// AA has already accepted the request
		return bs.utils.NewError(http.StatusForbidden, "Assessor has already accepted the request")
	}

	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.Name = patch["name"].(string)
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.UserName = patch["userName"].(string)
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.Status = constant.PENDING

	// #4 Increment the assignedCount by 1
	batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.AssignedCount++

	// #5 Save the batch
	if err := bs.dao.UpdateBatchByID(rs, batchID, batch); err != nil {
		rs.Error(err)
		if err.Error() == "not found" {
			return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
		}
		return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
	}

	// #6 Cancel pending notification of AA if exists
	rs.Info("Cancel all pending request to AA")

	// #7 Notify AA
	rs.Info("notify AA")

	// INFO: step #6 and #7, written inside the batch apis

	return nil

}

// GetBatchByID : Service
func (bs *BatchService) GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) *models.Batch {
	batch, _ := bs.dao.GetBatchByID(rs, batchID, project)
	return batch
}

// GetBatch : Service
func (bs *BatchService) GetBatch(rs app.RequestScope, query bson.M, project bson.M) *models.Batch {
	batch, _ := bs.dao.GetBatch(rs, query, project)
	return batch
}

// GetOneBatch :
func (bs *BatchService) GetOneBatch(rs app.RequestScope) (b *models.Batch, err error) {
	return bs.dao.FindOneBatch(rs)
}

// GetBatches :
func (bs *BatchService) GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchCout int, err error) {
	// Project and Select query in bson.M
	return bs.dao.GetBatches(rs, query, project)
	// return
}

// UpdateBatchByID : service
func (bs *BatchService) UpdateBatchByID(rs app.RequestScope, batchID int64, data interface{}) *models.Error {
	err := bs.dao.UpdateBatchByID(rs, batchID, data)
	if err == nil {
		return nil
	}
	if err != nil && err.Error() == "not found" {
		return bs.utils.NewError(http.StatusNotFound, "Batch ID or QPCode is invalid")
	}
	return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
}

// UpdateBatchWithFindQuery :
func (bs *BatchService) UpdateBatchWithFindQuery(rs app.RequestScope, query, update interface{}) *models.Error {
	err := bs.dao.UpdateBatchWithFindQuery(rs, query, update)
	if err == nil {
		return nil
	}
	if err.Error() == "not found" {
		return bs.utils.NewError(http.StatusNotFound, "BatchId or username is invalid")
	}
	return bs.utils.NewError(http.StatusInternalServerError, "Internal Server Error")
}

// GetBatchBasedOnCandidatesStatus :
func (bs *BatchService) GetBatchBasedOnCandidatesStatus(rs app.RequestScope) (batchDetails []*models.Batch, batchCount int, err error) {

	query := bson.M{"candidates": bson.M{"$elemMatch": bson.M{"userName": rs.GetJWTClaims().UserName,
		"userRole": rs.GetJWTClaims().Role, "status": rs.GetParams()["status"]}}}

	batchDetails, batchCount, err = bs.dao.GetBatches(rs, query, nil)
	return
}

func (bs *BatchService) CancelBatch(rs app.RequestScope) error {

	updateQuery := bson.M{"isCancelled": true}
	batchId, _ := strconv.Atoi(rs.GetParams()["batchId"])
	queryErr := bs.dao.UpdateBatchByID(rs, int64(batchId), updateQuery)
	return queryErr
}
