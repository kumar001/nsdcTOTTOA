package services

import (
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"
	"strings"
)

type adminDAO interface {
	GetAdminConfiguration(app.RequestScope, []string) (models.TOTConfiguration, error)
	GetAdminConfigurationToa(app.RequestScope, []string) (models.TOAConfiguration, error)
}

// AdminService :
type AdminService struct {
	dao adminDAO
}

// NewAdminService : creates instance of admin service
func NewAdminService(dao adminDAO) *AdminService {
	return &AdminService{dao}
}

// GetAdminConfiguration :
func (as *AdminService) GetAdminConfiguration(rs app.RequestScope, projectedFields []string) (models.TOTConfiguration, error) {
	return as.dao.GetAdminConfiguration(rs, projectedFields)
}

// GetAdminConfigurationToa ...Toa Admin config
func (as *AdminService) GetAdminConfigurationToa(rs app.RequestScope, projectedFields []string) (models.TOAConfiguration, error) {
	return as.dao.GetAdminConfigurationToa(rs, projectedFields)
}

// CalculateFees :
func (as *AdminService) CalculateFees(rs app.RequestScope, p models.PostData) (int, error) {
	projectedFields := make([]string, 3)
	projectedFields = append(projectedFields, "maxBatchSize")
	projectedFields = append(projectedFields, "trainingFees")
	projectedFields = append(projectedFields, "assessmentFeePerCandidate")
	totConfiguration, err := as.dao.GetAdminConfiguration(rs, projectedFields)
	trainingFees, _ := GetTrainingAssesmentFees(p, totConfiguration)
	if len(p.JobRoles) == 1 {
		feesAmount := trainingFees
		return feesAmount, err
	}
	feesAmount := trainingFees + feesBasedOnJObRole(p, totConfiguration)
	return feesAmount, err
}

// GetTrainingAssesmentFees :Get Base Prize for The Batch Based on 1st Job Role
func GetTrainingAssesmentFees(p models.PostData, totconfig models.TOTConfiguration) (int, int) {
	var trainingFees, AssesmentFees int
	if p.Type == "Training of Trainer-New" || p.Type == "Training of Assessor-New" {
		if strings.ToLower(p.JobRoles[0].QpParamOne.ParamDesc) == "technical" {
			trainingFees, AssesmentFees = totconfig.TrainingFees.NewTrainers.Technical, totconfig.AssessmentFeePerCandidate.Technical
		}
		if strings.ToLower(p.JobRoles[0].QpParamOne.ParamDesc) == "non-technical" {
			trainingFees, AssesmentFees = totconfig.TrainingFees.NewTrainers.NonTechnical, totconfig.AssessmentFeePerCandidate.NonTechnical
		}
	}
	if p.Type == "Training of Trainer-Existing" || p.Type == "Training of Assessor-Existing" {
		if strings.ToLower(p.JobRoles[0].QpParamOne.ParamDesc) == "technical" {
			trainingFees, AssesmentFees = totconfig.TrainingFees.ExistingTrainers.Technical, totconfig.AssessmentFeePerCandidate.Technical
		}
		if strings.ToLower(p.JobRoles[0].QpParamOne.ParamDesc) == "non-technical" {
			trainingFees, AssesmentFees = totconfig.TrainingFees.ExistingTrainers.NonTechnical, totconfig.AssessmentFeePerCandidate.NonTechnical
		}
	}
	// @Return Base Prize Based on First Job Role
	return trainingFees, AssesmentFees
}

func feesBasedOnJObRole(p models.PostData, totconfig models.TOTConfiguration) int {
	techTrainingFees, nonTechTrainingFees := totconfig.AssessmentFeePerCandidate.Technical, totconfig.AssessmentFeePerCandidate.NonTechnical
	return calcJobRoleCount(p)["Non-Technical"]*nonTechTrainingFees + calcJobRoleCount(p)["Technical"]*techTrainingFees
}

// CalcJobRoleCount  ...@params {jobRole :Technical or Non-Technical} Counting The JobRoles
func calcJobRoleCount(p models.PostData) map[string]int {
	jobRoleCount := map[string]int{}
	// Ommitting the 1st JobRole For Base Prize is added to the Batch Fees
	jobRoleCountArray := p.JobRoles[1:]
	for i := 0; i < len(jobRoleCountArray); i++ {
		_, ok := jobRoleCount[jobRoleCountArray[i].QpParamOne.ParamDesc]
		if !ok {
			jobRoleCount[jobRoleCountArray[i].QpParamOne.ParamDesc] = 0
		}
		jobRoleCount[jobRoleCountArray[i].QpParamOne.ParamDesc]++
	}
	return jobRoleCount
}
