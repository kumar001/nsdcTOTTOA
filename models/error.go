package models

import (
	"fmt"
)

// Error : custom error type
type Error struct {
	Status  int    `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
}

// String :
func (e *Error) String() string {
	return fmt.Sprintf(`{"status": %d, "message": "%s"}`, e.Status, e.Message)
}
