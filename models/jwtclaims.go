package models

// JWTClaims : this struct should be identical to the Authrization struct(authservice)
type JWTClaims struct {
	UserName                  string `json:"userName"`
	Role                      string `json:"role"`
	FirstName                 string `json:"firstName"`
	Email                     string `json:"email"`
	Status                    string `json:"status"`
	HasFilledRegistrationInfo bool   `json:"hasFilledRegistrationInfo"`
}
