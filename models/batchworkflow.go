package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

const (
	// CollectionBatchWorkflow : collection name
	CollectionBatchWorkflow = "batchworkflow"
)

// BatchWorkflow : BatchWorkFlow Collection
type BatchWorkflow struct {
	ID               bson.ObjectId `json:"_id" bson:"_id"`
	ReqID            int64         `json:"reqId,omitempty" bson:"reqId,omitempty"`
	ToUserRole       string        `json:"toUserRole,omitempty" bson:"toUserRole,omitempty"`
	UserName         string        `json:"userName,omitempty" bson:"userName,omitempty"`
	BatchID          int64         `json:"batchId,omitempty" bson:"batchId,omitempty"`
	QPCode           string        `json:"qpCode,omitempty" bson:"qpCode,omitempty"` // QPCode will empty, if the ToUserRole is TC
	Status           string        `json:"status,omitempty" bson:"status,omitempty"`
	Comment          string        `json:"comment,omitempty" bson:"comment,omitempty"`                   // optional
	AssignedTo       string        `json:"assignedTo,omitempty" bson:"assignedTo,omitempty"`             // this prop will contain the userName of the Assessor
	ParentWorkflowID bson.ObjectId `json:"parentWorkflowID,omitempty" bson:"parentWorkflowID,omitempty"` // this field will be available if ToUserRole is Assessor, Need to update the AA workflow, if Assessor takes any action
	ActionTakenOn    time.Time     `json:"actionTakenOn,omitempty" bson:"actionTakenOn,omitempty"`       // updated on
	CreatedOn        time.Time     `json:"createdOn,omitempty" bson:"createdOn,omitempty"`
	UpdatedOn        time.Time     `json:"updatedOn,omitempty" bson:"updatedOn,omitempty"`
}
