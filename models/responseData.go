package models

// ResponseData .. is used for Pagination Logic
type ResponseData struct {
	Count int         `json:"count"`
	Data  interface{} `json:"data"`
}
