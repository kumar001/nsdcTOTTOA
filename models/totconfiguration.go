package models

const (
	TOTConfigurationCollection = "totconfigurations"
)

type TOTConfiguration struct {
	MaxBatchSize                                           int                 `json:"maxBatchSize" bson:"maxBatchSize"`
	MaxDaysForRegistrationBeforeBatchStart                 int                 `json:"maxDaysForRegistrationBeforeBatchStart" bson:"maxDaysForRegistrationBeforeBatchStart"`
	MaxDaysToTakeActionByTCOnCandidate                     int                 `json:"maxDaysToTakeActionByTCOnCandidate" bson:"maxDaysToTakeActionByTCOnCandidate"`
	MaxDaysToTakeActionByAAOnCandidate                     int                 `json:"maxDaysToTakeActionByAAOnCandidate" bson:"maxDaysToTakeActionByAAOnCandidate"`
	MaxDaysToTakeActionBySSCOnCandidate                    int                 `json:"maxDaysToTakeActionBySSCOnCandidate" bson:"MaxDaysToTakeActionBySSCOnCandidate"`
	ValidityForTempTC                                      int                 `json:"validityForTempTC" bson:"validityForTempTC"`
	MaxDaysForBatchCreationBeforeBatchStart                int                 `json:"maxDaysForBatchCreationBeforeBatchStart" bson:"maxDaysForBatchCreationBeforeBatchStart"`
	MaxdaysToTakeActiononBatchByTC                         int                 `json:"maxdaysToTakeActiononBatchByTC" bson:"maxdaysToTakeActiononBatchByTC"`
	MaxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC int                 `json:"maxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC" bson:"maxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC"`
	MaxDaysToTakeActionOnBatchBySSC                        int                 `json:"maxDaysToTakeActionOnBatchBySSC" bson:"maxDaysToTakeActionOnBatchBySSC"`
	TrainingFees                                           TRFee               `json:"trainingFees" bson:"trainingFees"`
	MaxDaysToCollectFeesBeforeBatchStartDate               int                 `json:"maxDaysToCollectFeesBeforeBatchStartDate" bson:"maxDaysToCollectFeesBeforeBatchStartDate"`
	MaxDaysToAssignMTBySSCBeforeBatchStartDate             int                 `json:"maxDaysToAssignMTBySSCBeforeBatchStartDate" bson:"maxDaysToAssignMTBySSCBeforeBatchStartDate"`
	ReminderIntervalToSSCForAssignMT                       int                 `json:"reminderIntervalToSSCForAssignMT" bson:"reminderIntervalToSSCForAssignMT"`
	MaxdaysToTakeActiononBatchByMT                         int                 `json:"maxdaysToTakeActiononBatchByMT" bson:"maxdaysToTakeActiononBatchByMT"`
	MaxDaysToAssignAABySSCBeforeBatchStartDate             int                 `json:"maxDaysToAssignAABySSCBeforeBatchStartDate" bson:"maxDaysToAssignAABySSCBeforeBatchStartDate"`
	MaxdaysToTakeActiononBatchByAA                         int                 `json:"maxdaysToTakeActiononBatchByAA" bson:"MaxdaysToTakeActiononBatchByAA"`
	MaxDaysToAssignAssessorByAABeforebatchStartDate        int                 `json:"maxDaysToAssignAssessorByAABeforebatchStartDate" bson:"maxDaysToAssignAssessorByAABeforebatchStartDate"`
	MaxDaysToTakeActiononBatchByAssessor                   int                 `json:"maxDaysToTakeActiononBatchByAssessor" bson:"maxDaysToTakeActiononBatchByAssessor"`
	MaxDaysToUploadResultAfterBatchCompletion              int                 `json:"maxDaysToUploadResultAfterBatchCompletion" bson:"maxDaysToUploadResultAfterBatchCompletion"`
	MaxdaysToCertificationAfterBatchCompletion             int                 `json:"maxdaysToCertificationAfterBatchCompletion" bson:"maxdaysToCertificationAfterBatchCompletion"`
	TrainerCertificateValidity                             int                 `json:"trainerCertificateValidity" bson:"trainerCertificateValidity"`
	AssessmentFeePerCandidate                              AssessmentFee       `json:"assessmentFeePerCandidate" bson:"assessmentFeePerCandidate"`
	MinTrainingDays                                        MinimumTrainingDays `json:"minTrainingDays" bson:"minTrainingDays"`
}

type TRFee struct {
	ExistingTrainers ExistingTrainerFeeStructure `json:"existingTrainers" bson:"existingTrainers"`
	NewTrainers      NewtrainersFeeStructure     `json:"newTrainers" bson:"newTrainers"`
}

type ExistingTrainerFeeStructure struct {
	NonTechnical int `json:"nonTechnical" bson:"nonTechnical"`
	Technical    int `json:"technical" bson:"technical"`
}

type AssessmentFee struct {
	NonTechnical int `json:"nonTechnical" bson:"nonTechnical"`
	Technical    int `json:"technical" bson:"technical"`
}
type NewtrainersFeeStructure struct {
	NonTechnical int `json:"nonTechnical" bson:"nonTechnical"`
	Technical    int `json:"technical" bson:"technical"`
}

type PostData struct {
	Type     string                 `json:"type"`
	JobRoles []CalculateFeesJobRole `json:"jobRoles"`
}
type CalculateFeesJobRole struct {
	Qpcode     string     `json:"qpCode"`
	Version    string     `json:"version"`
	NsqfLevel  string     `json:"nsqfLevel"`
	QpParamOne QpParamOne `json:"qpParamOne"`
}
type QpParamOne struct {
	ParamID   string `json:"paramID"`
	ParamDesc string `json:"paramDesc"`
}

type MinimumTrainingDays struct {
	TrainingOfTrainerExisting TrainingDays `json:"Training of Trainer-Existing" bson:"Training of Trainer-Existing"`
	TrainingOfTrainerNew      TrainingDays `json:"Training of Trainer-New" bson:"Training of Trainer-New"`
}

type TrainingDays struct {
	MinTrainingDays   int `json:"minTrainingDays" bson:"minTrainingDays"`
	MinAssessmentDays int `json:"minAssesmentDays" bson:"minAssesmentDays"`
}
