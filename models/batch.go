package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// BatchSchema .... Represents Batches
const (
	CollectionBatch     = "batch"
	CollectionBatchType = "batchType"
	IDPrefix            = "BA"
)

// type details struct {
// 	UserName      string `json:"userName,omitempty" bson:"userName,omitempty"`
// 	Name          string `json:"name,omitempty" bson:"name,omitempty"`
// 	AssignedCount int    `json:"assignedCount,omitempty" bson:"assignedCount,omitempty"`
// }

// JobRole ...
type JobRole struct {
	ID                   bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	JobName              string        `json:"jobName,omitempty" bson:"jobName,omitempty"`
	QpCode               string        `json:"qpCode,omitempty" bson:"qpCode,omitempty"` // jobRole code
	Version              string        `json:"version,omitempty" bson:"version,omitempty"`
	MasterTrainerDetails struct {
		UserName      string `json:"userName,omitempty" bson:"userName,omitempty"`
		Name          string `json:"name,omitempty" bson:"name,omitempty"`
		AssignedCount int    `json:"assignedCount,omitempty" bson:"assignedCount,omitempty"` // how many time MT has been assigned for this jobrole
		Status        string `json:"status,omitempty" bson:"status,omitempty"`               // PENDING, ACCEPTED, REJECTED
		ReqID         int64  `json:"reqId,omitempty" bson:"reqId,omitempty"`                 // Batchworkflow req ID
		// *details
	} `json:"masterTrainerDetails,omitempty" bson:"masterTrainerDetails,omitempty"`

	AssessmentAgencyDetails struct {
		UserName        string   `json:"userName,omitempty" bson:"userName,omitempty"`
		Name            string   `json:"name,omitempty" bson:"name,omitempty"`
		AssignedCount   int      `json:"assignedCount,omitempty" bson:"assignedCount,omitempty"` // how many time MT has been assigned for this jobrole
		Status          string   `json:"status,omitempty" bson:"status,omitempty"`               // PENDING, ACCEPTED, REJECTED
		ReqID           int64    `json:"reqId,omitempty" bson:"reqId,omitempty"`                 // Batchworkflow req ID
		AssessorDetails struct { // this field will be updated by the AA
			Name          string `json:"name,omitempty" bson:"name,omitempty"`
			UserName      string `json:"userName,omitempty" bson:"userName,omitempty"`
			AssignedCount int    `json:"assignedCount,omitempty" bson:"assignedCount,omitempty"` // how many time ASSESSOR has been assigned for this jobrole
			Status        string `json:"status,omitempty" bson:"status,omitempty"`               // PENDING, ACCEPTED, REJECTED
			ReqID         int64  `json:"reqId,omitempty" bson:"reqId,omitempty"`                 // Batchworkflow req ID
		} `json:"assessorDetails,omitempty" bson:"assessorDetails,omitempty"`
	} `json:"assessmentAgencyDetails,omitempty" bson:"assessmentAgencyDetails,omitempty"`
}

// Batch ....
type Batch struct {
	ID                  bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	IsCancelled         *bool         `json:"isCancelled,omitempty" bson:"isCancelled"`
	BatchID             int64         `json:"batchId,omitempty" bson:"batchId,omitempty"`
	BatchFee            int64         `json:"batchFee,omitempty" bson:"batchFee,omitempty"`
	BatchName           string        `json:"batchName,omitempty" bson:"batchName,omitempty"`
	CreatedBy           string        `json:"createdBy,omitempty" bson:"createdBy,omitempty"` // SSC Username
	Status              string        `json:"status,omitempty" bson:"status,omitempty"`       // SSCSubmited Or TcApprovoal or Closed Batch
	Type                string        `json:"type,omitempty" bson:"type,omitempty"`
	BatchStartDate      time.Time     `json:"batchStartDate,omitempty" bson:"batchStartDate,omitempty"`
	BatchEndDate        time.Time     `json:"batchEndDate,omitempty" bson:"batchEndDate,omitempty"`
	Hours               int           `json:"hours,omitempty" bson:"hours,omitempty"`
	Size                int           `json:"size,omitempty" bson:"size,omitempty"`
	Sector              *sector       `json:"sector,omitempty" bson:"sector,omitempty"`
	SubSectors          *subSectors   `json:"subSectors,omitempty" bson:"subSectors,omitempty"`
	JobRoles            []*JobRole    `json:"jobRoles,omitempty" bson:"jobRoles,omitempty"`
	TCID                string        `json:"tcId,omitempty" bson:"tcId,omitempty"`
	TCName              string        `json:"tcName,omitempty" bson:"tcName,omitempty"`
	TCApproved          string        `json:"tcApproved" bson:"tcApproved"`               // if TC has approved, set to true
	TCReqID             int64         `json:"tcReqId,omitempty" bson:"tcReqId,omitempty"` // batchworkflow Id
	TCAssignedCount     int           `json:"tcAssignedCount,omitempty" bson:"tcAssignedCount,omitempty"`
	TPID                string        `json:"tpId,omitempty" bson:"tpId,omitempty"`
	TPName              string        `json:"tpName,omitempty" bson:"tpName,omitempty"`
	AssessmentStartDate time.Time     `json:"assessmentStartDate,omitempty" bson:"assessmentStartDate,omitempty"`
	AssessmentEndDate   time.Time     `json:"assessmentEndDate,omitempty" bson:"assessmentEndDate,omitempty"`
	TrainingStartDate   time.Time     `json:"trainingStartDate,omitempty" bson:"trainingStartDate,omitempty"`
	TrainingEndDate     time.Time     `json:"trainingEndDate,omitempty" bson:"trainingEndDate,omitempty"`
	Stage               int           `json:"stage,omitempty" bson:"stage,omitempty"`
	MaxBatchBudget      int           `json:"maxBatchBudget,omitempty" bson:"MaxBatchBudget,omitempty"`
	CreatedOn           time.Time     `json:"createdOn,omitempty" bson:"createdOn,omitempty"`
	UpdatedOn           time.Time     `json:"updatedOn,omitempty" bson:"updatedOn,omitempty"`
	Candidates          []*Candidate  `json:"candidates" bson:"candidates"`
	Announced           bool          `json:"announced" bson:"announced"` // batch announced
	// AppliedCandidates     []*Candidate  `json:"appliedCandidate,omitempty" bson:"appliedCandidate,omitempty"`
	// ShortListedCandidates []*Candidate  `json:"shortListedCandidates,omitempty" bson:"shortListedCandidates,omitempty"`
	Document string `json:"document,omitempty" bson:"document,omitempty"`
	Address  struct {
		AddressLine string `json:"addressLine,omitempty" form:"addressLine" bson:"addressLine,omitempty"`
		Landmark    string `json:"landmark,omitempty" form:"landmark" bson:"landmark,omitempty"`
		Pincode     string `json:"pincode,omitempty" form:"pincode" bson:"pincode,omitempty"`
		Zone        string `json:"zone,omitempty" bson:"zone,omitempty" form:"zone"`
		Country     struct {
			Name string `json:"name,omitempty"  bson:"countryName,omitempty" form:"name"`
			ID   int32  `json:"id,omitempty" bson:"id,omitempty" form:"id"`
		} `json:"country,omitempty"  bson:"country,omitempty" form:"country"`
		State struct {
			Name string `json:"name,omitempty"  bson:"name,omitempty" form:"name"`
			ID   int32  `json:"id,omitempty" bson:"id,omitempty" form:"id"`
		} `json:"state,omitempty"  bson:"state,omitempty" form:"state"`
		District struct {
			Name string `json:"name,omitempty"  bson:"name,omitempty" form:"name"`
			ID   int32  `json:"id,omitempty" bson:"id,omitempty" form:"id"`
		} `json:"district,omitempty"  bson:"district,omitempty" form:"district"`
		SubDistrict struct {
			Name string `json:"name,omitempty"  bson:"name,omitempty" form:"name"`
			ID   int32  `json:"id,omitempty" bson:"id,omitempty" form:"id"`
		} `json:"subDistrict,omitempty" form:"subDistrict" bson:"subDistrict,omitempty"`
		AddressProof struct {
			Type        string `json:"type,omitempty"  bson:"type,omitempty" form:"type"`
			LocationURL string `json:"locationURL,omitempty"  bson:"locationURL,omitempty" form:"locationURL"`
		} `json:"addressProof,omitempty" form:"addressProof" bson:"addressProof,omitempty"`
	} `json:"address,omitempty" form:"address" bson:"address,omitempty"`
}

// Candidate ... Candidate for the batch
type Candidate struct {
	Name     string     `json:"name,omitempty" bson:"name"`
	UserName string     `json:"userName" bson:"userName"`
	UserRole string     `json:"userRole,omitempty" bson:"userRole"`
	Email    string     `json:"email,omitempty" bson:"email"`
	Phone    int64      `json:"phone,omitempty" bson:"phone"`
	Status   string     `json:"status,omitempty" bson:"status"`
	DOB      *time.Time `json:"dob" bson:"dob"`

	TCStatus   string `json:"tcStatus,omitempty" bson:"tcStatus"` // PENDING|ACCEPTED|REJECTED TC will shortlist
	TCRemarks  string `json:"tcRemarks" bson:"tcRemarks"`
	SSCRemarks string `json:"sscRemarks" bson:"sscRemarks"`
	SSCStatus  string `json:"sscStatus,omitempty" bson:"sscStatus"` // PENDING|ACCEPTED|REJECTED // SSC will enroll

	TCRespondedOn  *time.Time `json:"tcRespondedOn,omitempty" bson:"tcRespondedOn"`
	SSCRespondedOn *time.Time `json:"sscRespondedOn,omitempty" bson:"sscRespondedOn"`
	AppliedOn      *time.Time `json:"appliedOn,omitempty" bson:"appliedOn"`
}

type sector struct {
	ID   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
}

type subSectors struct {
	// ID   string `json:"id" bson:"id"` // BALU
	// Name string `json:"name" bson:"name"`
	SubSectorID   string `json:"subSectorID" bson:"subSectorID"`
	SubSectorName string `json:"subSectorName" bson:"subSectorName"`
}

// BatchType ...
type BatchType struct {
	ID   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
}
