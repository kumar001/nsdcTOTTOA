package models

const (
	TOAConfigurationCollection = "toaconfigurations"
)

type TOAConfiguration struct {
	MaxBatchSize                                           int                   `json:"maxBatchSize" bson:"maxBatchSize"`
	MaxDaysForRegistrationBeforeBatchStart                 int                   `json:"maxDaysForRegistrationBeforeBatchStart" bson:"maxDaysForRegistrationBeforeBatchStart"`
	MaxDaysToTakeActionByTCOnCandidate                     int                   `json:"maxDaysToTakeActionByTCOnCandidate" bson:"maxDaysToTakeActionByTCOnCandidate"`
	MaxDaysToTakeActionByAAOnCandidate                     int                   `json:"maxDaysToTakeActionByAAOnCandidate" bson:"maxDaysToTakeActionByAAOnCandidate"`
	MaxDaysToTakeActionBySSCOnCandidate                    int                   `json:"maxDaysToTakeActionBySSCOnCandidate" bson:"MaxDaysToTakeActionBySSCOnCandidate"`
	ValidityForTempTC                                      int                   `json:"validityForTempTC" bson:"validityForTempTC"`
	MaxDaysForBatchCreationBeforeBatchStart                int                   `json:"maxDaysForBatchCreationBeforeBatchStart" bson:"maxDaysForBatchCreationBeforeBatchStart"`
	MaxDaysToAcceptBatchByTC                               int                   `json:"maxDaysToAcceptBatchByTC" bson:"maxDaysToAcceptBatchByTC"`
	MaxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC int                   `json:"maxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC" bson:"maxDaysBeforebatchStartDateForBatchSubmissionToTCBySSC"`
	MaxDaysToTakeActionOnBatchBySSC                        int                   `json:"maxDaysToTakeActionOnBatchBySSC" bson:"maxDaysToTakeActionOnBatchBySSC"`
	AssesmentFees                                          ARFee                 `json:"assesmentFees" bson:"assesmentFees"`
	MaxDaysToCollectFeesBeforeBatchStartDate               int                   `json:"maxDaysToCollectFeesBeforeBatchStartDate" bson:"maxDaysToCollectFeesBeforeBatchStartDate"`
	MaxDaysToAssignMTBySSCBeforeBatchStartDate             int                   `json:"maxDaysToAssignMTBySSCBeforeBatchStartDate" bson:"maxDaysToAssignMTBySSCBeforeBatchStartDate"`
	ReminderIntervalToSSCForAssignMT                       int                   `json:"reminderIntervalToSSCForAssignMT" bson:"reminderIntervalToSSCForAssignMT"`
	MaxdaysToTakeActiononBatchByMT                         int                   `json:"maxdaysToTakeActiononBatchByMT" bson:"maxdaysToTakeActiononBatchByMT"`
	MaxDaysToAssignAABySSCBeforeBatchStartDate             int                   `json:"maxDaysToAssignAABySSCBeforeBatchStartDate" bson:"maxDaysToAssignAABySSCBeforeBatchStartDate"`
	MaxdaysToTakeActiononBatchByAA                         int                   `json:"maxdaysToTakeActiononBatchByAA" bson:"MaxdaysToTakeActiononBatchByAA"`
	MaxDaysToAssignAssessorByAABeforebatchStartDate        int                   `json:"maxDaysToAssignAssessorByAABeforebatchStartDate" bson:"maxDaysToAssignAssessorByAABeforebatchStartDate"`
	MaxDaysToTakeActiononBatchByAssessor                   int                   `json:"maxDaysToTakeActiononBatchByAssessor" bson:"maxDaysToTakeActiononBatchByAssessor"`
	MaxDaysToUploadResultAfterBatchCompletion              int                   `json:"maxDaysToUploadResultAfterBatchCompletion" bson:"maxDaysToUploadResultAfterBatchCompletion"`
	MaxdaysToCertificationAfterBatchCompletion             int                   `json:"maxdaysToCertificationAfterBatchCompletion" bson:"maxdaysToCertificationAfterBatchCompletion"`
	TrainerCertificateValidity                             int                   `json:"trainerCertificateValidity" bson:"trainerCertificateValidity"`
	AssessmentFeePerCandidate                              AssessmentFee         `json:"assessmentFeePerCandidate" bson:"assessmentFeePerCandidate"`
	MinassesmentDays                                       *MinimumTrainingDaysA `json:"minTrainingDays" bson:"minTrainingDays"`
}

type ARFee struct {
	ExistingAssessors ExistingAssessorFeeStructure `json:"existingAssessor" bson:"existingAssessor"`
	NewAssessors      NewassessorsFeeStructure     `json:"newAssessor" bson:"newAssessor"`
}

type ExistingAssessorFeeStructure struct {
	NonTechnical int `json:"nonTechnical" bson:"nonTechnical"`
	Technical    int `json:"technical" bson:"technical"`
}

type NewassessorsFeeStructure struct {
	NonTechnical int `json:"nonTechnical" bson:"nonTechnical"`
	Technical    int `json:"technical" bson:"technical"`
}

type MinimumAssesmentDays struct {
	TrainingOfAssessorExisting TrainingDays `json:"Training of Assessor-Existing" bson:"Training of Assessor-Existing"`
	TrainingOfAssessorNew      TrainingDays `json:"Training of Assessor-New" bson:"Training of Assessor-New"`
}

type MinimumTrainingDaysA struct {
	TrainingOfTrainerExisting TrainingDays `json:"Training of Assessor-Existing" bson:"Training of Assessor-Existing"`
	TrainingOfAssessorNew     TrainingDays `json:"Training of Assessor-New" bson:"Training of Assessor-New"`
}
