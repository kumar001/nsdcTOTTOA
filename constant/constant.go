package constant

const (
	// AUTHSERVICE : Origin
	AUTHSERVICE = "http://localhost:3001"
	// USERSERVICE : Origin
	USERSERVICE = "http://localhost:3000"
)

const (
	// CONTEXTJWTKEY : Context key, all JWT claims will be stored here
	// this constant should be used to read the jwtClaims
	CONTEXTJWTKEY = "jwtClaims"
)

const (
	// MTMAXASSIGNCOUNT : Master Trainer Max Assign Count
	MTMAXASSIGNCOUNT = 3
	// AAMAXASSIGNCOUNT : Assessment Agency Max Assign Count
	AAMAXASSIGNCOUNT = 3
	// AMAXASSIGNCOUNT : Assessor Max Assign Count
	AMAXASSIGNCOUNT = 3
)

const (
	// TRAINER :
	TRAINER = "Trainer"

	// ASSESSMENTAGENCY :
	ASSESSMENTAGENCY = "Assessment Agency"

	// ASSESSOR :
	ASSESSOR = "Assessor"

	// TRAININGCENTRE :
	TRAININGCENTRE = "Training Centre"

	// SSC :
	SSC = "SSC"
)

const (
	// NEW :
	NEW = "NEW"
	// CANCELLEDBYSSC :
	CANCELLEDBYSSC = "CANCELLEDBYSSC"
	// PENDING :
	PENDING = "PENDING"
	// APPLIED :
	APPLIED = "APPLIED"
	// ACCEPTED :
	ACCEPTED = "ACCEPTED"

	// REJECTED :
	REJECTED = "REJECTED"
	// AUTOREJECTED ... Auto Reject {CronJob}
	AUTOREJECTED = "AUTOREJECTED"

	// ANNOUNCED :
	ANNOUNCED = "ANNOUNCED"
)

// BatchType : All available Batch Type
const (
	BatchType = "Training of Trainer-New,Training of Trainer-Existing,Training of Assessor-New,Training of Assessor-Existing"
)
