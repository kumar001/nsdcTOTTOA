package main

import (
	"fmt"
	"log"
	"net/http"
	"nsdctottoaservice/apis"
	"nsdctottoaservice/app"
	"nsdctottoaservice/daos"
	"nsdctottoaservice/services"
	"nsdctottoaservice/utils"
	"os"

	"nsdctottoaservice/middleware"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {

	// INFO: load app configs here
	if err := app.LoadConfig("./config"); err != nil {
		panic(fmt.Errorf("Invalid application configuration: %s", err))
	}

	// TODO: Connect to database, app level

	// TODO: Connect redis store, app level

	// TODO: Create logger instance? if required
	logger := logrus.New()

	// start the server
	address := fmt.Sprintf(":%v", app.Config.ServerPort)
	log.Printf("server %v is started at %v\n", app.Version, address)
	panic(http.ListenAndServe(address, buildRoutes(logger)))
}

func buildRoutes(l *logrus.Logger) *mux.Router {
	// create instance of mux router
	r := mux.NewRouter()

	utils := utils.GetUtils()

	m := middleware.GetMiddlewares(utils)

	r.Use(m.AllowCors)
	r.Use(m.HasLoggedIn)
	r.Methods("OPTIONS").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
		return
	})

	// Initialize not found handler
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
		w.Write([]byte("Resource not found"))
	})

	// Create Logger Instance
	logger := logrus.New()
	// Replace os.Stdout to some other io.Writer type
	logger.Out = os.Stdout
	// Set log level
	logger.SetLevel(logrus.DebugLevel)

	// set all logger configs here
	// log to file or stdout
	// Creates request scope

	r.Use(app.Init(logger))

	// Set path prefix/route group
	// Create subroute
	//v1 := r.PathPrefix("/v1").Subrouter()

	v1 := r.PathPrefix("/api/v1/tottoa").Subrouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprintf(w, "Hello World")
	}).Methods("GET")

	// userDao := daos.NewUserDao()
	// apis.ServeUserResource(v1, services.NewUserService(userDao))
	// Create instance of batchWorkflow DAO
	batchWorkflowDao := daos.GetBatchWorkflowDAO()
	batchWorkflowSvc := services.NewBatchWorkflowService(batchWorkflowDao, utils)
	// Create batchDao
	batchDao := daos.NewBatchDao()
	batchSvc := services.NewBatchService(batchDao, utils)
	apis.ServeBatchWorkflowResource(v1, batchWorkflowSvc, batchSvc)
	// Serve Batch resource
	// this service needs batchDao, utils and batchWorkflowDao
	apis.ServeBatchResource(v1, batchSvc, batchWorkflowSvc)

	//apis.ServeBatchResource(v11, batchSvc, batchWorkflowSvc)

	// Serve Admin resource
	adminDao := daos.NewAdminDao()
	apis.ServeAdminResource(v1, services.NewAdminService(adminDao))

	cronJobDao := daos.NewcronJobDao()
	cronJobService := services.NewCronJobServices(cronJobDao)

	adminSvc := services.NewAdminService(adminDao)
	apis.ServeCronJobResource(v1, cronJobService, adminSvc)
	return r
}
