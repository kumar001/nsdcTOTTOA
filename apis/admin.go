package apis

import (
	"encoding/json"
	"fmt"
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"nsdctottoaservice/utils"
	"strings"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

type adminService interface {
	GetAdminConfiguration(app.RequestScope, []string) (models.TOTConfiguration, error)
	GetAdminConfigurationToa(app.RequestScope, []string) (models.TOAConfiguration, error)
	CalculateFees(rs app.RequestScope, p models.PostData) (int, error)
}

type adminResource struct {
	service adminService
}

// ServeAdminResource sets up the routing of user endpoints and the corresponding handlers.
func ServeAdminResource(rg *mux.Router, service adminService) {
	r := &adminResource{service}

	rg.HandleFunc("/admin/configuration/{type}", r.GetAdminConfiguration).Methods("GET").Queries("fields", "{configurationTypes}")

	rg.HandleFunc("/admin/batch/fees", r.GetFees).Methods("POST")
	rg.HandleFunc("/admin/batch/type", r.GetBatchType).Methods("GET")
	rg.HandleFunc("/admin/batch/status", r.GetBatchStatus).Methods("GET")
	rg.HandleFunc("/admin/all/totconfig", r.GetAllTOTConfig).Methods("GET")
	rg.HandleFunc("/admin/all/toaconfig", r.GetAllTOAConfig).Methods("GET")
}

func (br *adminResource) GetAdminConfiguration(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	fields := strings.Split(r.URL.Query().Get("fields"), ",")

	if rs.GetParams()["type"] == "tot" {
		configValue, _ := br.service.GetAdminConfiguration(rs, fields)
		utils.With200(w, configValue)
		return
	}
	if rs.GetParams()["type"] == "toa" {
		configValue, _ := br.service.GetAdminConfigurationToa(rs, fields)
		utils.With200(w, configValue)
		return

	}

}
func (br *adminResource) GetFees(w http.ResponseWriter, r *http.Request) {

	rs := app.GetRequestScope(r)
	postDataRequest := &models.PostData{}
	if err := json.Unmarshal(rs.GetBody(), postDataRequest); err != nil {
		w.WriteHeader(400)
		rs.Error(err)
		fmt.Fprintf(w, "Invalid Data")
		return
	}
	fees, err := br.service.CalculateFees(rs, *postDataRequest)

	if err != nil {
		rs.Error(err)
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, fees)
	return

}
func (br *adminResource) GetBatchStatus(w http.ResponseWriter, r *http.Request) {

	var responseConstant = make(map[string][]string)
	responseConstant["BatchStatus"] = append(responseConstant["BatchStatus"], constant.NEW)
	responseConstant["BatchStatus"] = append(responseConstant["BatchStatus"], constant.CANCELLEDBYSSC)
	responseConstant["BatchStatus"] = append(responseConstant["BatchStatus"], constant.PENDING)
	responseConstant["BatchStatus"] = append(responseConstant["BatchStatus"], constant.ACCEPTED)
	responseConstant["BatchStatus"] = append(responseConstant["BatchStatus"], constant.REJECTED)

	utils.With200(w, responseConstant)

}

func (br *adminResource) GetBatchType(w http.ResponseWriter, r *http.Request) {
	var responseConstant = make(map[string][]string)
	constantValue := strings.Split(constant.BatchType, ",")
	responseConstant["batchType"] = constantValue
	utils.With200(w, responseConstant)
}

// used Only for Development
func (br *adminResource) GetAllTOTConfig(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	val := []interface{}{}
	err := rs.DB().C(models.TOTConfigurationCollection).Find(bson.M{}).Select(bson.M{}).All(&val)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, val)
	return
}

// used Only for Development
func (br *adminResource) GetAllTOAConfig(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	selectQuery := bson.M{"minTrainingDays": 1}
	val := []interface{}{}
	err := rs.DB().C(models.TOAConfigurationCollection).Find(bson.M{}).Select(selectQuery).All(&val)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, val)
	return
}
