package apis

import (
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"nsdctottoaservice/utils"
	"time"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

type CronJobService interface {
	UpDateBatches(rs app.RequestScope, findQuery, updateQuery bson.M) error
	UpDateBatchworkFlows(rs app.RequestScope, findQuery, updateQuery bson.M) error
	GetBatchIdsFromBwflow(rs app.RequestScope, findQuery bson.M) ([]int64, error)
}

type AdminSvc interface {
	GetAdminConfiguration(app.RequestScope, []string) (models.TOTConfiguration, error)
	GetAdminConfigurationToa(app.RequestScope, []string) (models.TOAConfiguration, error)
}
type cronJobResource struct {
	service      CronJobService
	adminService AdminSvc
}

// ServeCronJobResource  ... Serve Cron Job
func ServeCronJobResource(rg *mux.Router, service CronJobService, adminSvc AdminSvc) {
	r := &cronJobResource{service, adminSvc}
	rg.HandleFunc("/cron/batch/tc/reject", r.TCAutoRejectBatch).Methods("POST")
	rg.HandleFunc("/cron/batch/aa/reject", r.AAAutoRejectBatch).Methods("POST")

}

// AutoRejectBatch ...
// Batch should be accepted by TC within 3 (or ‘x’) days of receiving the Batch request or it will be auto-rejected
// for re-assignment by SSC.
func (cr *cronJobResource) TCAutoRejectBatch(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	totConfigFields := []string{"maxdaysToTakeActiononBatchByTC"}

	totConfigValue, err := cr.adminService.GetAdminConfiguration(rs, totConfigFields)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	bwFindQueryCreatedOn := bson.M{"$lt": time.Now().AddDate(0, 0, totConfigValue.MaxdaysToTakeActiononBatchByTC)}
	bwFindQuery := bson.M{"status": constant.NEW, "toUserRole": "Training Centre", "createdOn": bwFindQueryCreatedOn}
	batchIds, err := cr.service.GetBatchIdsFromBwflow(rs, bwFindQuery)

	if err != nil {
		utils.With500m(w, "error In Gettting BatchId"+err.Error())
		return
	}

	toUserRole := make([]string, len(batchIds))
	for index := range toUserRole {
		toUserRole[index] = constant.TRAININGCENTRE
	}
	batchFindQuery := bson.M{"batchId": bson.M{"$in": batchIds}}
	batchUpdateQuery := bson.M{"$set": bson.M{"tcApproved": constant.REJECTED}}
	err = cr.service.UpDateBatches(rs, batchFindQuery, batchUpdateQuery)
	if err != nil {
		utils.With500m(w, "DevError:BatchUpdation:"+err.Error())
		return
	}
	bwUpdateFindQuery := bson.M{"batchId": bson.M{"$in": batchIds}, "toUserRole": bson.M{"$in": toUserRole}}
	bwUpdateQuery := bson.M{"$set": bson.M{"status": constant.REJECTED, "comment": "Batch Is AutoRejected"}}
	err = cr.service.UpDateBatchworkFlows(rs, bwUpdateFindQuery, bwUpdateQuery)
	if err != nil {
		utils.With500m(w, "Dev:Error:BatchWorkflowUpdation"+err.Error())
		return
	}
	utils.With200(w, "Batch Rejected For Training Centre")
	return
}

// AAAutoRejectBatch ...
// Assessment Agency should be assigned by SSC 9 (or ‘x’) days prior Batch start date or else it will be auto-
// rejected for re-assignment by SSC.
func (cr *cronJobResource) AAAutoRejectBatch(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	bwFindQuery := bson.M{"status": constant.NEW, "toUserRole": constant.ASSESSMENTAGENCY}
	batchIds, err := cr.service.GetBatchIdsFromBwflow(rs, bwFindQuery)
	if err != nil {
		utils.With500m(w, "error In Gettting BatchId"+err.Error())
		return
	}
	totConfigFields := []string{"maxDaysToAssignAABySSCBeforeBatchStartDate"}
	totConfigValue, err := cr.adminService.GetAdminConfiguration(rs, totConfigFields)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	batchCreatedOnFindQuery := bson.M{"$lt": time.Now().AddDate(0, 0, totConfigValue.MaxDaysToAssignAABySSCBeforeBatchStartDate)}
	batchFindQuery := bson.M{"batchId": bson.M{"$in": batchIds}, "createdOn": batchCreatedOnFindQuery}
	batchUpdateQuery := bson.M{"$set": bson.M{"status": constant.REJECTED}}
	err = cr.service.UpDateBatches(rs, batchFindQuery, batchUpdateQuery)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, "Batch  Rejected For Assesment Agency")
	return
}
