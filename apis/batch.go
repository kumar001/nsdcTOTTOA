package apis

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"nsdctottoaservice/services"
	"nsdctottoaservice/utils"

	"strconv"
	"time"

	"io/ioutil"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/mux"
)

// type BatchApi struct {
// 	BatchService services.IbatchService
// }

// type batchService interface {
// 	CreateBatchSvc(app.RequestScope, *models.Batch) error
// 	AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error
// 	GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error)
// 	// GetAppliedCandidates(app.RequestScope, string) (*models.Batch, error)
// 	UpdateMTForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
// 	UpdateAAForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
// 	UpdateAssessorForJobRole(rs app.RequestScope, batchID int64, qpCode string, patch bson.M) *models.Error
// 	GetBatchBasedOnCandidatesStatus(rs app.RequestScope) ([]*models.Batch, int, error)

// 	UpdateBatchWithFindQuery(rs app.RequestScope, query, patch interface{}) *models.Error

// 	GetBatch(app.RequestScope, bson.M, bson.M) *models.Batch
// 	GetOneBatch(rs app.RequestScope) (b *models.Batch, err error)
// 	GetBatchTypes(rs app.RequestScope) ([]*models.BatchType, error)
// 	// GetBatch(rs app.RequestScope, batchID int64) *models.Batch
// 	GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchcount int, err error)
// 	CancelBatch(rs app.RequestScope) error
// }

// type batchworkflowSvc interface {
// 	Post(app.RequestScope, *models.BatchWorkflow) *models.Error
// 	CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) *models.Error
// }

type batchResource struct {
	service          services.IbatchService
	batchworkflowSvc services.IbatchworkflowSvc
}

// *****************************************************************************************************
// ******************************* Init routes and inject service, daos, utils *************************
// *****************************************************************************************************

// ServeBatchResource sets up the routing of user endpoints and the corresponding handlers.
func ServeBatchResource(rg *mux.Router, service services.IbatchService, bws services.IbatchworkflowSvc) {

	r := &batchResource{service, bws}

	rg.HandleFunc("/batch", r.CreateBatchHandler).Methods("POST")
	rg.HandleFunc("/batch", r.GetBatchesForActorsHandler).Methods("GET")
	rg.HandleFunc("/batch/{batchId}", r.GetOneBatch).Methods("GET")
	rg.HandleFunc("/batch/type/all", r.GetBatchTypes).Methods("GET")

	rg.HandleFunc("/batch/{batchId}/candidate", r.AddCandidateToBatchHandler).Methods("POST")
	rg.HandleFunc("/batch/{batchId}/candidate", r.GetCandidatesBasedOnStatusHandler).Queries("status", "{status}").Methods("GET")
	// rg.HandleFunc("/batch/{batchId}/getAppliedCandidates", r.GetAppliedCandidatesHandler).Methods("GET")

	rg.HandleFunc("/batch/{batchId}/candidates/{userName}", r.SetCandidateStatus).Methods("PATCH")

	rg.HandleFunc("/batch/{batchId}/jobRoles/masterTrainerDetails", r.UpdateMTForJobRole).Methods("PATCH")
	rg.HandleFunc("/batch/{batchId}/jobRoles/assessmentAgencyDetails", r.UpdateAAForJobRole).Methods("PATCH")
	rg.HandleFunc("/batch/{batchId}/jobRoles/assessmentAgencyDetails/assessor", r.UpdateAccessorForJobRole).Methods("PATCH")

	rg.HandleFunc("/batch/upcoming/", r.GetUpcomingBatches).Queries("status", "{status}", "sectorId", "{sectorId}", "subSectorId", "{subSectorId}", "qpCode", "{qpCode}", "stateId", "{stateId}", "districtId", "{districtId}", "subDistrictId", "{subDistrictId}", "type", "{type}", "batchStartDate", "{batchStartDate}", "batchEndDate", "{batchEndDate}", "pageNo", "{pageNo}", "itemsPerPage", "{itemsPerPage}").Methods("GET")

	rg.HandleFunc("/batch/candidate/", r.GetBatchBasedOnCandidatesStatus).Queries("status", "{status}", "itemsPerPage", "{itemsPerPage}", "pageNo", "{pageNo}").Methods("GET")
	rg.HandleFunc("/batch/tc/{tcId}", r.GetBatchBelongsToTC).Methods("GET")
	// CancelBatch
	rg.HandleFunc("/batch/cancel/{batchId}", r.CancelBatchHandler).Methods("PATCH")
}

// ****************************************************************************************************
// ***************************************** Handler functions ****************************************
// ****************************************************************************************************

func (br *batchResource) CreateBatchHandler(w http.ResponseWriter, r *http.Request) {
	// Create requestScope
	rs := app.GetRequestScope(r)

	// Batch instance
	batch := &models.Batch{}

	// Read body and unmarshal it into batch
	if err := json.Unmarshal(rs.GetBody(), batch); err != nil {
		rs.Error(err)
		w.WriteHeader(400)
		fmt.Fprintf(w, "Invalid Data")
		return
	}

	// Now we have the batch data

	// Assume batch is created and TC has been informed for the first time
	// FIXME: Update this value after the creation of BatchWorkflow for TC
	batch.TCAssignedCount++

	batch.TCApproved = constant.NEW

	// TODO: validate batch details

	// Call createBatchresource service
	if err := br.service.CreateBatchSvc(rs, batch); err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, err.Error())
		return
	}

	// TODO: Notify TC

	rs.Info("Notify TC")

	// TODO: wrap these Id inside a function
	bw := &models.BatchWorkflow{}
	bw.UserName = batch.TCID
	bw.ID = bson.NewObjectId()
	// TODO: Fetch it from constants
	bw.ToUserRole = constant.TRAININGCENTRE
	bw.BatchID = batch.BatchID
	// TODO: get it from constants
	bw.Status = constant.NEW
	bw.CreatedOn = time.Now()

	if err := br.batchworkflowSvc.Post(rs, bw); err != nil {
		rs.Error(err)
	} else {
		rs.Info("TC Notified")
	}

	// TODO: update this in batch collection

	if err := br.service.UpdateBatchWithFindQuery(rs, bson.M{"batchId": batch.BatchID}, bson.M{"$set": bson.M{"tcReqId": bw.ReqID}}); err != nil {
		rs.Warn("failed to update bw reqId")
		rs.Error(err)
	}

	w.WriteHeader(201)
	fmt.Fprintf(w, `{"message": "Created"}`)
	return
}

func (br *batchResource) UpdateMTForJobRole(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)

	/**
	 * Expected Payload
	 * {"userName": "USER_NAME_OF_MT", "name": "NAME_OF_THE_MT_TRAINER"}
	 */

	var patch bson.M

	params := rs.GetParams()

	batchID, _ := strconv.ParseInt(params["batchId"], 10, 64)

	// unmarshall the req body into patch
	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}

	qpCode := patch["qpCode"].(string)

	if err := br.service.UpdateMTForJobRole(rs, batchID, qpCode, patch); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, err.String())
		return
	}

	// Cancel all pending wf
	if err := br.batchworkflowSvc.CancelPendingWorkflow(rs, batchID, qpCode, constant.TRAINER, patch["userName"].(string)); err != nil {
		rs.Error(err)
		rs.Info("Failed to cancel all pending wfs")
	}
	rs.Info("Cancelled all pending wfs")

	// TODO: wrap this code inside a function

	bw := &models.BatchWorkflow{}
	bw.UserName = patch["userName"].(string) // MT UserName
	bw.ID = bson.NewObjectId()
	// TODO: Fetch it from constants

	bw.QPCode = qpCode
	bw.ToUserRole = constant.TRAINER
	bw.BatchID = batchID
	// TODO: get it from constants
	bw.Status = constant.NEW
	bw.CreatedOn = time.Now()

	if err := br.batchworkflowSvc.Post(rs, bw); err != nil {
		// TODO: Implement retry mechanism or notify admin
		rs.Error(err)
		rs.Info("MT Not Notified")
	} else {
		rs.Info("MT Notified")
	}

	// store req id in batch collection
	q := bson.M{"batchId": batchID, "jobRoles.qpCode": bw.QPCode}
	u := bson.M{"jobRoles.$.masterTrainerDetails.reqId": bw.ReqID}

	if err := br.service.UpdateBatchWithFindQuery(rs, q, bson.M{"$set": u}); err != nil {
		rs.Warn("failed to update bw MT reqId")
		rs.Error(err)
	}

	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "OK"}`)
}

// UpdateAAForJobRole :
func (br *batchResource) UpdateAAForJobRole(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)

	/**
	 * Expected Payload
	 * {"userName": "USER_NAME_OF_AA", "name": "NAME_OF_THE_AA"}
	 */

	var patch bson.M

	params := rs.GetParams()

	batchID, _ := strconv.ParseInt(params["batchId"], 10, 64)

	// unmarshall the req body into patch
	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(400)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}
	qpCode := patch["qpCode"].(string)
	if err := br.service.UpdateAAForJobRole(rs, batchID, qpCode, patch); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, err.String())
		return
	}

	// Cancel all pending wf
	if err := br.batchworkflowSvc.CancelPendingWorkflow(rs, batchID, qpCode, constant.ASSESSMENTAGENCY, patch["userName"].(string)); err != nil {
		rs.Error(err)
		rs.Info("Failed to cancel all pending wfs")
	}
	rs.Info("Cancelled all pending wfs")

	// TODO: wrap this code inside a function

	bw := &models.BatchWorkflow{}
	bw.UserName = patch["userName"].(string) // AA UserName
	bw.ID = bson.NewObjectId()
	// TODO: Fetch it from constants
	bw.QPCode = qpCode
	bw.ToUserRole = constant.ASSESSMENTAGENCY
	bw.BatchID = batchID
	// TODO: get it from constants
	bw.Status = constant.NEW
	bw.CreatedOn = time.Now()

	if err := br.batchworkflowSvc.Post(rs, bw); err != nil {
		// TODO: Implement retry mechanism or notify admin
		rs.Error(err)
		rs.Info("AA Not Notified")
	} else {
		rs.Info("AA Notified")
	}

	// store req id in batch collection
	q := bson.M{"batchId": batchID, "jobRoles.qpCode": bw.QPCode}
	u := bson.M{"jobRoles.$.assessmentAgencyDetails.reqId": bw.ReqID}

	if err := br.service.UpdateBatchWithFindQuery(rs, q, bson.M{"$set": u}); err != nil {
		rs.Warn("failed to update bw AA reqId")
		rs.Error(err)
	}

	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "OK"}`)
}

func (br *batchResource) UpdateAccessorForJobRole(w http.ResponseWriter, r *http.Request) {

	rs := app.GetRequestScope(r)

	/**
	 * Expected Payload
	 * {"userName": "USER_NAME_OF_AA", "name": "NAME_OF_THE_AA"}
	 */

	var patch bson.M

	params := rs.GetParams()

	batchID, _ := strconv.ParseInt(params["batchId"], 10, 64)

	// unmarshall the req body into patch
	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)

		w.WriteHeader(400)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}
	qpCode := patch["qpCode"].(string)
	if err := br.service.UpdateAssessorForJobRole(rs, batchID, qpCode, patch); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, err.String())
		return
	}

	// Cancel all pending wf
	// FIXME: userName should not be the in update query
	if err := br.batchworkflowSvc.CancelPendingWorkflow(rs, batchID, qpCode, constant.ASSESSOR, patch["userName"].(string)); err != nil {
		rs.Error(err)
		rs.Info("Failed to cancel all pending wfs")
	}
	rs.Info("Cancelled all pending wfs")

	// Notify Assessor
	bw := &models.BatchWorkflow{}
	bw.UserName = patch["userName"].(string) // AA UserName
	bw.ID = bson.NewObjectId()
	// TODO: Fetch it from constants

	bw.QPCode = qpCode
	bw.ToUserRole = constant.ASSESSOR
	bw.BatchID = batchID
	// TODO: get it from constants
	bw.Status = constant.NEW
	bw.CreatedOn = time.Now()

	if err := br.batchworkflowSvc.Post(rs, bw); err != nil {
		// TODO: Implement retry mechanism or notify admin
		rs.Error(err)
		rs.Info("Assessor Not Notified")
	} else {
		rs.Info("Assessor Notified")
	}

	// store req id in batch collection
	q := bson.M{"batchId": batchID, "jobRoles.qpCode": bw.QPCode}
	u := bson.M{"jobRoles.$.assessmentAgencyDetails.assessorDetails.reqId": bw.ReqID}

	if err := br.service.UpdateBatchWithFindQuery(rs, q, bson.M{"$set": u}); err != nil {
		rs.Warn("failed to update bw Assessor reqId")
		rs.Error(err)
	}

	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "OK"}`)
}

// AddCandidateToBatchHandler :
func (br *batchResource) AddCandidateToBatchHandler(w http.ResponseWriter, r *http.Request) {
	// Create requestScope
	rs := app.GetRequestScope(r)
	role := rs.GetJWTClaims().Role
	// role = constant.ASSESSOR
	userName := rs.GetJWTClaims().UserName
	// if role != constant.TRAINER {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	fmt.Fprintf(w, `{"message": "Bad Request"}`)
	// 	return
	// }

	token := r.Header.Get("Authorization")
	h := make(map[string]string)
	h["Authorization"] = token

	params := rs.GetParams()

	batchID, _ := strconv.ParseInt(params["batchId"], 10, 64)
	// Batch instance
	candidate := &models.Candidate{}
	userService := services.UsersService{}
	var result io.ReadCloser
	var err error

	if role == constant.TRAINER {
		result, err = userService.GetUserBasedOnUserName(userName, "trainer", h)
	}
	if role == constant.ASSESSOR {
		result, err = userService.GetUserBasedOnUserName(userName, "assessor", h)
	}
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, `{"message": "User Not Found"}`)
		return
	}

	c, _ := ioutil.ReadAll(result)
	err1 := json.Unmarshal(c, &candidate)

	if err1 != nil {
		fmt.Println(err1, "ERR1")
	}
	candidate.UserRole = role
	candidate.Status = constant.APPLIED
	candidate.TCStatus = constant.PENDING
	candidate.SSCStatus = constant.PENDING

	if err := br.service.AddCandidateToBatch(rs, batchID, candidate); err != nil {
		w.WriteHeader(500)
		rs.Error(err)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Error())
		return
	}

	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "%v"}`, "Candidate Added to batch")
	return
}

func (br *batchResource) GetCandidatesBasedOnStatusHandler(w http.ResponseWriter, r *http.Request) {

	// Create requestScope
	rs := app.GetRequestScope(r)
	params := rs.GetParams()

	batchID, _ := strconv.ParseInt(params["batchId"], 10, 64)

	queries := rs.GetQueries()
	fmt.Println(queries)
	status := queries.Get("status")
	fmt.Println(status, "DEFGHJ")
	// Call AddCandidateToBatch service
	response, err := br.service.GetCandidatesBasedOnStatus(rs, batchID, status)
	if err != nil {
		fmt.Fprintf(w, "Err")
		return
	}

	batchData, _ := json.Marshal(response)

	w.Write(batchData)
}

// SetCandidateStatus : set candidate status
func (br *batchResource) SetCandidateStatus(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	batchID, err := strconv.ParseInt(rs.GetParams()["batchId"], 10, 64)
	if err != nil {
		rs.Warn(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Error())
		return
	}
	cID := rs.GetParams()["userName"]
	var patch bson.M

	// fmt.Printf("%+v", rs.GetQueries())
	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Error())
		return
	}
	/**
	 * if role = "Training Centre", update will be {isShortlisted: true, shortlistedOn: time.Now()}
	 * if role = "SSC" update object will look like {isEnrolled: true, enrolledOn: time.Now()}
	 */
	query := bson.M{"batchId": batchID, "candidates.userName": cID}
	// update := bson.M{"$set": bson.M{"candidates.$.status": patch["status"]}}
	var update bson.M
	if rs.GetJWTClaims().Role == constant.TRAININGCENTRE {
		update = bson.M{"$set": bson.M{"candidates.$.tcStatus": patch["status"], "candidates.$.tcRespondedOn": rs.Now(), "tcRemarks": patch["remarks"]}}
	} else if rs.GetJWTClaims().Role == constant.SSC {
		update = bson.M{"$set": bson.M{"candidates.$.sscStatus": patch["status"], "candidates.$.sscRespondedOn": rs.Now(), "sscRemarks": patch["remarks"]}}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, `{"message": "Unauthorized"}`)
		return
	}
	// fmt.Println(query, update)
	if err := br.service.UpdateBatchWithFindQuery(rs, query, update); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
		return
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "done"}`)
	return
}

func (br *batchResource) GetUpcomingBatches(w http.ResponseWriter, r *http.Request) {

	rs := app.GetRequestScope(r)
	var responseData models.ResponseData
	var err error
	responseData.Data, responseData.Count, err = br.service.GetBatches(rs, utils.FindQueryForUpComingBatch(rs), bson.M{})
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, responseData)
	return
}

func (br *batchResource) GetOneBatch(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	batchData, err := br.service.GetOneBatch(rs)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, batchData)
	return
}

// GetBatchesForActorsHandler :
func (br *batchResource) GetBatchesForActorsHandler(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	// if rs.GetJWTClaims().Role != constant.SSC {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	fmt.Fprintf(w, `{"message": "Bad Request"}`)
	// 	return
	// }

	status := rs.GetQueries().Get("status")
	var q interface{}

	userName := rs.GetJWTClaims().UserName

	switch rs.GetJWTClaims().Role {
	case constant.SSC:
		q = getBatchForSSCQuery(userName, status)
	case constant.TRAINER:
		q = getBatchForMTQuery(userName, status)
	case constant.ASSESSMENTAGENCY:
		q = getBatchForAAQuery(userName, status)
	case constant.ASSESSOR:
		q = getBatchForAssessorQuery(userName, status)
	case constant.TRAININGCENTRE:
		q = getBatchForTCQuery(userName, status)
	default:
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Not allowed")
		return
	}

	resp := make(map[string]interface{})
	var err error

	// TODO: dont use error, use modlels.Error

	resp["batches"], resp["count"], err = br.service.GetBatches(rs, q.(bson.M), bson.M{})

	if err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"message": "Internal Server Error"}`)
		return
	}

	utils.With200(w, resp)
	return
}

func getBatchForSSCQuery(userName, status string) interface{} {
	type l []bson.M
	switch status {
	case constant.ANNOUNCED:
		return bson.M{"createdBy": userName, "announced": true}
	case constant.PENDING:
		return bson.M{"createdBy": userName, "$or": l{
			bson.M{"jobRoles.masterTrainerDetails": bson.M{"$exists": false}},
			bson.M{"jobRoles.masterTrainerDetails.userName": bson.M{"$exists": false}},
			bson.M{"jobRoles.assessmentAgencyDetails": bson.M{"$exists": false}},
			bson.M{"jobRoles.assessmentAgencyDetails.userName": bson.M{"$exists": false}},
			bson.M{"tcApproved": constant.NEW},
			bson.M{"jobRoles.masterTrainerDetails.status": constant.PENDING},
			bson.M{"jobRoles.assessmentAgencyDetails.status": constant.PENDING},
		}}
	case constant.REJECTED:
		return bson.M{"createdBy": userName, "$or": l{
			bson.M{"tcApproved": constant.REJECTED},
			bson.M{"jobRoles.masterTrainerDetails.status": constant.REJECTED},
			bson.M{"jobRoles.assessmentAgencyDetails.status": constant.REJECTED},
			bson.M{"jobRoles.assessmentAgencyDetails.assessorDetails.status": constant.REJECTED},
		}}

	default:
		return bson.M{"createdBy": userName}
	}
}

func getBatchForTCQuery(userName, status string) interface{} {
	// type list []interface{}
	switch status {
	case constant.NEW:
		return bson.M{"tcId": userName, "tcApproved": "NEW"}
	case constant.ACCEPTED:
		return bson.M{"tcId": userName, "tcApproved": "ACCEPTED"}
	case constant.PENDING:
		return bson.M{"tcId": userName, "candidates.tcStatus": "PENDING"}
	default:
		// handles all status
		return bson.M{"tcId": userName}
	}
}

func getBatchForMTQuery(userName, status string) interface{} {
	switch status {
	case constant.PENDING:
		return bson.M{"tcApproved": constant.ACCEPTED,
			"jobRoles": bson.M{"$elemMatch": bson.M{
				"masterTrainerDetails.userName": userName,
				"masterTrainerDetails.status":   constant.PENDING,
			}}}
	case constant.ACCEPTED:
		return bson.M{"tcApproved": constant.ACCEPTED,
			"jobRoles": bson.M{"$elemMatch": bson.M{
				"masterTrainerDetails.userName": userName,
				"masterTrainerDetails.status":   constant.ACCEPTED,
			}}}
	default:
		return bson.M{"tcApproved": constant.ACCEPTED, "jobRoles.masterTrainerDetails.userName": userName, "jobRoles.masterTrainerDetails.status": "PENDING"}
	}
}

func getBatchForAAQuery(userName, status string) interface{} {
	switch status {
	case constant.PENDING:
		return bson.M{"tcApproved": constant.ACCEPTED,
			"jobRoles": bson.M{"$elemMatch": bson.M{
				"assessmentAgencyDetails.userName": userName,
				"assessmentAgencyDetails.status":   constant.PENDING,
			}}}
	case constant.ACCEPTED:
		return bson.M{"tcApproved": constant.ACCEPTED,
			"jobRoles": bson.M{"$elemMatch": bson.M{
				"assessmentAgencyDetails.userName": userName,
				"assessmentAgencyDetails.status":   constant.ACCEPTED,
			}}}
	default:
		return bson.M{"tcApproved": constant.ACCEPTED, "jobRoles.assessmentAgencyDetails.userName": userName, "jobRoles.assessmentAgencyDetails.status": "PENDING"}
	}
}

func getBatchForAssessorQuery(userName, status string) interface{} {
	switch status {
	case constant.PENDING:
		return bson.M{"jobRoles": bson.M{"$elemMatch": bson.M{
			"assessmentAgencyDetails.status":                   constant.ACCEPTED,
			"assessmentAgencyDetails.assessorDetails.userName": userName,
			"assessmentAgencyDetails.assessorDetails.status":   constant.PENDING,
		}}}
	case constant.ACCEPTED:
		return bson.M{"jobRoles": bson.M{"$elemMatch": bson.M{
			"assessmentAgencyDetails.assessorDetails.userName": userName,
			"assessmentAgencyDetails.assessorDetails.status":   constant.ACCEPTED,
		}}}
	default:
		return bson.M{"jobRoles": bson.M{"$elemMatch": bson.M{
			"assessmentAgencyDetails.status":                   constant.ACCEPTED,
			"assessmentAgencyDetails.assessorDetails.userName": userName,
			"assessmentAgencyDetails.assessorDetails.status":   constant.PENDING,
		}}}
	}
}

// @prams Candidates {Actor} Trainer , assessor,
func (br *batchResource) GetBatchBasedOnCandidatesStatus(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	var responseData models.ResponseData
	var err error
	responseData.Data, responseData.Count, err = br.service.GetBatchBasedOnCandidatesStatus(rs)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, responseData)
	return
}

// @params Only For Developement
// GetBatchBelongsToTC :
func (br *batchResource) GetBatchBelongsToTC(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	query := []bson.M{
		{"$match": bson.M{
			"userName": rs.GetParams()["tcId"],
		}},
		{"$lookup": bson.M{
			"from":         "batch",
			"localField":   "tcId",
			"foreignField": "userName",
			"as":           "BatchDetails",
		}}}
	result := []bson.M{}

	err := rs.DB().C("trainingcentre").Pipe(query).All(&result)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, result)
	return
}

// CancelBatchHandler @params {BatchId}
func (br *batchResource) CancelBatchHandler(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	err := br.service.CancelBatch(rs)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, "Batch Cancelled")
	return
}

func (br *batchResource) GetBatchTypes(w http.ResponseWriter, r *http.Request) {

	rs := app.GetRequestScope(r)
	batchTypes, err := br.service.GetBatchTypes(rs)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, batchTypes)
	return

}
