package apis

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"nsdctottoaservice/services"
	"nsdctottoaservice/utils"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

// type batchworkflowService interface {
// 	GetBatchWorkflow(r app.RequestScope, v map[string]string) ([]*models.BatchWorkflow, error)
// 	UpdateBatchWorkflow(rs app.RequestScope, reqID int64, patch bson.M) *models.Error
// 	GetOne(rs app.RequestScope, query, project bson.M) (*models.BatchWorkflow, *models.Error)
// 	GetPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) ([]bson.M, int, error)
// 	GetPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error)
// }

// type batchSvcInBW interface {
// 	GetBatch(rs app.RequestScope, query bson.M, project bson.M) *models.Batch
// 	UpdateBatchByID(rs app.RequestScope, batchID int64, data interface{}) *models.Error
// }

type batchworkflowResource struct {
	service      services.IbatchworkflowService
	batchService services.IbatchSvcInBW
}

// ServeBatchWorkflowResource :
func ServeBatchWorkflowResource(rg *mux.Router, service services.IbatchworkflowService, batchSvc services.IbatchSvcInBW) {
	r := &batchworkflowResource{service, batchSvc}

	rg.HandleFunc("/batchworkflow", r.GetBatchWorkflows).Queries("pageNo", "{pageNo}", "itemsPerPage", "{itemsPerPage}").Methods("GET")
	rg.HandleFunc("/batchworkflow/trainingcentre/{reqId}", r.PostBWActionByTC).Methods("PATCH")
	rg.HandleFunc("/batchworkflow/trainer/{reqId}", r.PostBWActionByMT).Methods("PATCH")
	rg.HandleFunc("/batchworkflow/assessmentagency/{reqId}", r.PostBWActionByAA).Methods("PATCH")
	rg.HandleFunc("/batchworkflow/assessor/{reqId}", r.PostBWActionByAssessor).Methods("PATCH")

	rg.HandleFunc("/batchworkflow/pending/", r.GetPendingBatchesHandler).Queries("pageNo", "{pageNo}", "itemsPerPage", "{itemsPerPage}", "status", "{status}").Methods("GET")
	rg.HandleFunc("/batchworkflow/pending/{id}", r.GetPendingBatchHandler).Methods("GET")
}

func (bw *batchworkflowResource) GetBatchWorkflows(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	loggedInuserDetails := rs.GetJWTClaims()
	// FIXME: dont need to pass JWT claims in params
	// get it from requestScope, i.e. rs.GetJWTClaims()
	tokenValue := map[string]string{
		"userName": loggedInuserDetails.UserName,
		"role":     loggedInuserDetails.Role,
	}
	// paginate := rs.GetParams()
	batchWorkflowData, err := bw.service.GetBatchWorkflow(rs, tokenValue)

	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	// FIXME: dont use utils package directly, inject util func(s)? in resource or service
	// for now use native function, w.WriteHeader() and fmt.Fprintf()
	utils.With200(w, batchWorkflowData)
	return
}

func (bw *batchworkflowResource) PostBWActionByTC(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	patch := bson.M{}
	params := rs.GetParams()
	reqID, err := strconv.ParseInt(params["reqId"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Invalid Data"}`)
		return
	}

	/**
	 * Expected Data: {"status": "ACCEPTED|REJECTED", "comment": ""}
	 */

	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}

	var err1 *models.Error
	var batchWorkflow *models.BatchWorkflow

	// get batch to read the batchID from it
	if batchWorkflow, err1 = bw.service.GetOne(rs, bson.M{"reqId": reqID}, bson.M{}); err1 != nil {
		rs.Error(err1)
		w.WriteHeader(err1.Status)
		fmt.Fprintf(w, `{"message": "%v"}`, err1.String())
		return
	}

	// if batchWorkflow

	if batchWorkflow.Status != constant.NEW {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, "Action not required")
		return
	}

	batch := bw.batchService.GetBatch(rs, bson.M{"batchId": batchWorkflow.BatchID}, bson.M{})
	if batch == nil {
		rs.Warn("Invalid BatchID")
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, `{"message": "%v"}`, "invalid batch")
		return
	}

	// Check if the user is authorized to make this request

	// if batch.TCID != batchWorkflow.UserName {
	// 	// unauthorized
	// 	w.WriteHeader(http.StatusUnauthorized)
	// 	rs.Warn("Unauthorized Request")
	// 	fmt.Fprintf(w, "Unauthorized Request, assigned user name mismatched")
	// 	return
	// }

	// if rs.GetJWTClaims().UserName != batchWorkflow.UserName || rs.GetJWTClaims().Role != constant.TRAININGCENTRE {
	// 	w.WriteHeader(http.StatusUnauthorized)
	// 	rs.Warn("Unauthorized Request, role or userName mismatched")
	// 	fmt.Fprintf(w, "Unauthorized Request")
	// 	return
	// }

	if err := bw.service.UpdateBatchWorkflow(rs, reqID, patch); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
		return
	}

	// Workflow updated
	// now update batch
	if patch["status"].(string) == constant.ACCEPTED {
		batch.TCApproved = constant.ACCEPTED
		// INFO: JUST FOR NOW
		batch.Announced = true
	} else {
		batch.TCApproved = constant.REJECTED
		batch.Announced = false
	}

	if err := bw.batchService.UpdateBatchByID(rs, batch.BatchID, batch); err != nil {
		rs.Error(err)
		w.WriteHeader(err.Status)
		fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
		return
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "%v"}`, "updated")
}

// PostBWActionByMT :
func (bw *batchworkflowResource) PostBWActionByMT(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	patch := bson.M{}

	params := rs.GetParams()
	idsS := strings.Replace(params["reqId"], " ", "", -1)

	var ids []int64
	for _, val := range strings.Split(idsS, ",") {
		reqID, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			rs.Error(err)
			continue
		}
		ids = append(ids, reqID)
	}

	// reqID, err := strconv.ParseInt(params["reqId"], 10, 64)
	if len(ids) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, "Invalid Data")
		return
	}

	/**
	 * Expected Data: {"status": "ACCEPTED|REJECTED", "comment": ""}
	 */

	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}

	var err1 *models.Error
	var batchWorkflow *models.BatchWorkflow

	// get batch to read the batchID from it
	for _, reqID := range ids {
		if batchWorkflow, err1 = bw.service.GetOne(rs, bson.M{"reqId": reqID}, bson.M{}); err1 != nil {
			w.WriteHeader(err1.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err1.String())
			return
		}

		if batchWorkflow.Status != constant.NEW {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, `{"message": "%v"}`, "Action not required")
			return
		}

		// find batch query and projection|selection
		bQ := bson.M{"batchId": batchWorkflow.BatchID, "jobRoles.qpCode": batchWorkflow.QPCode}
		sel := bson.M{}

		// Get the batch details
		batch := bw.batchService.GetBatch(rs, bQ, sel)

		// check if the batch is not empty
		if batch == nil {
			rs.Warn("Invalid BatchID")
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, `{"message": "%v"}`, "invalid batch")
			return
		}

		var jobRoleIndex = -1
		// Check if the qpCode exists in the jobRoles
		for i, val := range batch.JobRoles {
			if val.QpCode == batchWorkflow.QPCode {
				jobRoleIndex = i
				break
			}
		}

		// Check if the user is authorized to make this request

		// if batch.JobRoles[jobRoleIndex].MasterTrainerDetails.UserName != batchWorkflow.UserName {
		// 	// unauthorized
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request")
		// 	fmt.Fprintf(w, "Unauthorized Request, assigned user name mismatched")
		// 	return
		// }

		// if rs.GetJWTClaims().UserName != batchWorkflow.UserName || rs.GetJWTClaims().Role != constant.TRAINER {
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request, role or userName mismatched")
		// 	fmt.Fprintf(w, "Unauthorized Request")
		// 	return
		// }

		if err := bw.service.UpdateBatchWorkflow(rs, reqID, patch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}

		// Workflow updated

		// Check Status
		if patch["status"].(string) == constant.ACCEPTED {
			batch.JobRoles[jobRoleIndex].MasterTrainerDetails.Status = constant.ACCEPTED
		} else {
			batch.JobRoles[jobRoleIndex].MasterTrainerDetails.Status = constant.REJECTED
		}

		// Update the batch
		if err := bw.batchService.UpdateBatchByID(rs, batch.BatchID, batch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "%v"}`, "updated")
}

// PostBWActionByAA :
func (bw *batchworkflowResource) PostBWActionByAA(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	patch := bson.M{}

	params := rs.GetParams()
	idsS := strings.Replace(params["reqId"], " ", "", -1)

	var ids []int64
	for _, val := range strings.Split(idsS, ",") {
		reqID, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			rs.Error(err)
			continue
		}
		ids = append(ids, reqID)
	}

	// reqID, err := strconv.ParseInt(params["reqId"], 10, 64)
	if len(ids) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, "Invalid Data")
		return
	}

	/**
	 * Expected Data: {"status": "ACCEPTED|REJECTED", "comment": ""}
	 */

	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}

	var err1 *models.Error
	var batchWorkflow *models.BatchWorkflow

	for _, reqID := range ids {

		// get batch to read the batchID from it
		if batchWorkflow, err1 = bw.service.GetOne(rs, bson.M{"reqId": reqID}, bson.M{}); err1 != nil {
			w.WriteHeader(err1.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err1.String())
			return
		}

		if batchWorkflow.Status != constant.NEW {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, `{"message": "%v"}`, "Action not required")
			return
		}

		// find batch query and projection|selection
		bQ := bson.M{"batchId": batchWorkflow.BatchID, "jobRoles.qpCode": batchWorkflow.QPCode}
		sel := bson.M{}

		// Get the batch details
		batch := bw.batchService.GetBatch(rs, bQ, sel)

		var jobRoleIndex = -1
		// Check if the qpCode exists in the jobRoles
		for i, val := range batch.JobRoles {
			if val.QpCode == batchWorkflow.QPCode {
				jobRoleIndex = i
				break
			}
		}

		// check if the batch is not empty
		if batch == nil {
			rs.Warn("Invalid BatchID")
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, `{"message": "%v"}`, "invalid batch")
			return
		}

		// Check if the user is authorized to make this request

		// if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.UserName != batchWorkflow.UserName {
		// 	// unauthorized
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request")
		// 	fmt.Fprintf(w, "Unauthorized Request, assigned user name mismatched")
		// 	return
		// }

		// if rs.GetJWTClaims().UserName != batchWorkflow.UserName || rs.GetJWTClaims().Role != constant.ASSESSMENTAGENCY {
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request, role or userName mismatched")
		// 	fmt.Fprintf(w, "Unauthorized Request")
		// 	return
		// }

		if err := bw.service.UpdateBatchWorkflow(rs, reqID, patch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}

		// Workflow updated

		// Check Status
		if patch["status"].(string) == constant.ACCEPTED {
			batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.Status = constant.ACCEPTED
		} else {
			batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.Status = constant.REJECTED
		}

		// Update the batch
		if err := bw.batchService.UpdateBatchByID(rs, batch.BatchID, batch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "%v"}`, "updated")
}

// PostBWActionByAssessor :
func (bw *batchworkflowResource) PostBWActionByAssessor(w http.ResponseWriter, r *http.Request) {
	// Create requestScope
	rs := app.GetRequestScope(r)
	patch := bson.M{}

	params := rs.GetParams()

	idsS := strings.Replace(params["reqId"], " ", "", -1)

	var ids []int64
	for _, val := range strings.Split(idsS, ",") {
		reqID, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			rs.Error(err)
			continue
		}
		ids = append(ids, reqID)
	}

	// reqID, err := strconv.ParseInt(params["reqId"], 10, 64)
	if len(ids) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "%v"}`, "Invalid Data")
		return
	}

	/**
	 * Expected Data: {"status": "ACCEPTED|REJECTED", "comment": ""}
	 */

	if err := json.Unmarshal(rs.GetBody(), &patch); err != nil {
		rs.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"message": "Bad Request"}`)
		return
	}

	var err1 *models.Error
	var batchWorkflow *models.BatchWorkflow

	// get batch to read the batchID from it and for validation
	for _, reqID := range ids {

		if batchWorkflow, err1 = bw.service.GetOne(rs, bson.M{"reqId": reqID}, bson.M{}); err1 != nil {
			w.WriteHeader(err1.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err1.String())
			return
		}

		if batchWorkflow.Status != constant.NEW {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, `{"message": "%v"}`, "Action not required")
			return
		}

		// find batch query and projection|selection
		bQ := bson.M{"batchId": batchWorkflow.BatchID, "jobRoles.qpCode": batchWorkflow.QPCode}
		sel := bson.M{}

		// Get the batch details
		batch := bw.batchService.GetBatch(rs, bQ, sel)

		// check if the batch is not empty
		if batch == nil {
			rs.Warn("Invalid BatchID")
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, `{"message": "%v"}`, "invalid batch")
			return
		}

		var jobRoleIndex = -1
		// Check if the qpCode exists in the jobRoles
		for i, val := range batch.JobRoles {
			if val.QpCode == batchWorkflow.QPCode {
				jobRoleIndex = i
				break
			}
		}

		// Check if the user is authorized to make this request

		// if batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.UserName != batchWorkflow.UserName {
		// 	// unauthorized
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request")
		// 	fmt.Fprintf(w, "Unauthorized Request, user name mismatched")
		// 	return
		// }

		// if rs.GetJWTClaims().UserName != batchWorkflow.UserName || rs.GetJWTClaims().Role != constant.ASSESSOR {
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	rs.Warn("Unauthorized Request, role or userName mismatched")
		// 	fmt.Fprintf(w, "Unauthorized Request")
		// 	return
		// }

		if err := bw.service.UpdateBatchWorkflow(rs, reqID, patch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}

		// Workflow updated

		// Check Status
		if patch["status"].(string) == constant.ACCEPTED {
			batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.Status = constant.ACCEPTED
		} else {
			batch.JobRoles[jobRoleIndex].AssessmentAgencyDetails.AssessorDetails.Status = constant.REJECTED
		}

		// Update the batch
		if err := bw.batchService.UpdateBatchByID(rs, batch.BatchID, batch); err != nil {
			rs.Error(err)
			w.WriteHeader(err.Status)
			fmt.Fprintf(w, `{"message": "%v"}`, err.Message)
			return
		}
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, `{"message": "%v"}`, "updated")
}

// Pending Batches for Actor

func (bw *batchworkflowResource) GetPendingBatchesHandler(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	var ResponseDataValue models.ResponseData
	params := rs.GetParams()
	var err error
	ResponseDataValue.Data, ResponseDataValue.Count, err = bw.service.GetPendingBatches(rs, rs.GetJWTClaims(), params)
	log.Println("--------batch", err)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, ResponseDataValue)
	return
}

// GetPendingBatchHandler ... Get One Pending Batch with batchworflow @params {batchId}
func (bw *batchworkflowResource) GetPendingBatchHandler(w http.ResponseWriter, r *http.Request) {
	rs := app.GetRequestScope(r)
	responseData, err := bw.service.GetPendingBatch(rs)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}
	utils.With200(w, responseData)
	return
}
