package apis

import (
	"net/http"
	"testing"
)

func Test_batchResource_SetCandidateStatus(t *testing.T) {
	type fields struct {
		service          batchService
		batchworkflowSvc batchworkflowSvc
	}
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
	// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			br := &batchResource{
				service:          tt.fields.service,
				batchworkflowSvc: tt.fields.batchworkflowSvc,
			}
			br.SetCandidateStatus(tt.args.w, tt.args.r)
		})
	}
}
