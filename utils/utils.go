package utils

import (
	"fmt"
	"net/http"
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Utils :
type Utils struct {
}

// GetUtils :
func GetUtils() *Utils {
	return &Utils{}
}

// JSONStrToBsonM : converts json string to bson map
// IDEA: consider pass
func (u *Utils) JSONStrToBsonM(jsonStr string) bson.M {
	var v bson.M
	if jsonStr == "" {
		jsonStr = `{}`
	}
	if err := bson.UnmarshalJSON([]byte(jsonStr), &v); err != nil {
		fmt.Println("helpers/JSONStrToBsonM: ", err)
		fmt.Println("WARN: JSONStrToBsonM: func will return empty bson map")
	}
	return v
}

/**
* Counter
 */
const (
	collectionCounter = "counters"
)

type counter struct {
	Key   string `json:"key,omitempty" bson:"key,omitempty" form:"key"`
	Value int64  `json:"value,omitempty" bson:"value,omitempty" form:"value"`
}

// NextCount : this function returns next value
func (u *Utils) NextCount(rs app.RequestScope, key string) int64 {
	result := counter{}
	if _, err := rs.DB().C(collectionCounter).Find(bson.M{"key": key}).Apply(mgo.Change{
		Update:    bson.M{"$set": bson.M{"key": key}, "$inc": bson.M{"value": 1}},
		Upsert:    true,
		ReturnNew: true,
	}, &result); err != nil {
		rs.Warn("Autoincrement error(1):", err.Error())
		return 0
	}
	return result.Value
}

// HTTPGet :  HTTPGet method
func (u *Utils) HTTPGet(url string, h map[string]string) (resp *http.Response, e error) {
	client := &http.Client{}
	req, err1 := http.NewRequest("GET", url, nil)
	if err1 != nil {
		return nil, err1
	}
	for key, val := range h {
		req.Header.Add(key, val)
	}
	return client.Do(req)
}

// NewError :
func (u *Utils) NewError(status int, message interface{}) *models.Error {
	msg := ""
	switch message.(type) {
	case error:
		msg = message.(error).Error()
		break
	case string:
		msg = message.(string)
		break
	default:
		msg = "Unknown error"
	}
	return &models.Error{Status: status, Message: msg}
}

// FindQueryForUpComingBatch .... Upcoming Batches for the Actor: TRAINER ||  ASSESSOR
func FindQueryForUpComingBatch(rs app.RequestScope) (query bson.M) {
	apiQueryParams := rs.GetParams()
	batchStartDateIST := ConvertStringIst(apiQueryParams["batchStartDate"])
	batchEndDateIST := ConvertStringIst(apiQueryParams["batchEndDate"])
	if len(apiQueryParams) == 0 {
		query = nil
	} else {
		stateIdInt, _ := strconv.Atoi(apiQueryParams["stateId"])
		districtIdInt, _ := strconv.Atoi(apiQueryParams["districtId"])
		subDistrictIdInt, _ := strconv.Atoi(apiQueryParams["subDistrictId"])
		query = bson.M{"type": apiQueryParams["type"]}
		query["jobRoles"] = bson.M{"$elemMatch": bson.M{"qpCode": apiQueryParams["qpCode"]}}
		query["batchStartDate"] = bson.M{"$gte": batchStartDateIST}
		query["batchEndDate"] = bson.M{"$lte": batchEndDateIST}
		query["status"] = apiQueryParams["status"]
		query["sector.id"] = apiQueryParams["sectorId"]
		query["address.state.id"] = int32(stateIdInt)
		query["address.district.id"] = int32(districtIdInt)
		query["address.subDistrict.id"] = int32(subDistrictIdInt)
		query["tcApproved"] = constant.ACCEPTED
		candidatesOrQuery := []bson.M{}
		candidatesExists := bson.M{"candidates": bson.M{"$elemMatch": bson.M{"userName": bson.M{"$exists": false}}}}
		candidatesNeQuery := bson.M{"candidates": bson.M{"$elemMatch": bson.M{"userName": bson.M{"$ne": rs.GetJWTClaims().UserName}}}}
		candidatesArraySize := bson.M{"candidates": bson.M{"$size": 0}}
		query["$or"] = append(candidatesOrQuery, candidatesExists, candidatesNeQuery, candidatesArraySize)
	}
	return
}

// CovertStringToISTime ...@params{time} Datatype String Convert String Time to IST Format {2018-05-17 20:04:05 IST}
func CovertStringToISTime(t string) time.Time {
	IST, _ := time.LoadLocation("Asia/Kolkata")
	const longForm = "2006-01-02 15:04:05 MST"
	afterConvertedtoIst, _ := time.ParseInLocation(longForm, t, IST)
	return afterConvertedtoIst
}

// ConvertStringIst  ... Parsing the time Based on the Location @Params Input ISOstring {"2018-12-24T10:33:53.626Z"}
func ConvertStringIst(t string) time.Time {
	parsedTime, _ := time.Parse(time.RFC3339, t)
	countryLocation, _ := time.LoadLocation("Asia/Kolkata")
	return parsedTime.In(countryLocation)
}
