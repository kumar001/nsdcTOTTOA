package daos

import (
	"errors"
	"fmt"
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

// BatchDAO :
type BatchDAO struct {
}

type IbatchDAO interface {
	CreateBatch(app.RequestScope, *models.Batch) error
	AddCandidateToBatch(app.RequestScope, int64, *models.Candidate) error
	GetCandidatesBasedOnStatus(app.RequestScope, int64, string) ([]bson.M, error)
	// GetAppliedCandidates(app.RequestScope, string) (*models.Batch, error)
	GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) (*models.Batch, error)
	GetBatch(rs app.RequestScope, query bson.M, peoject bson.M) (*models.Batch, error)
	UpdateBatchByID(rs app.RequestScope, batchID int64, update interface{}) error
	UpdateBatchWithFindQuery(rs app.RequestScope, query, update interface{}) error
	FindOneBatch(rs app.RequestScope) (b *models.Batch, err error)
	GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchcount int, err error)
	FindBatchTypes(rs app.RequestScope) (b []*models.BatchType, err error)
}

// NewBatchDao : create instance of batchdao
func NewBatchDao() *BatchDAO {
	return &BatchDAO{}
}

// CreateBatch : creates a batch
func (dao *BatchDAO) CreateBatch(rs app.RequestScope, batch *models.Batch) error {
	return rs.DB().C(models.CollectionBatch).Insert(batch)
}

// AddCandidateToBatch : add candidate to a batch
func (dao *BatchDAO) AddCandidateToBatch(rs app.RequestScope, batchID int64, candidate *models.Candidate) error {
	var b *models.Batch
	err := rs.DB().C(models.CollectionBatch).Find(bson.M{"batchId": batchID, "candidates.userName": candidate.UserName, "candidates.userRole": candidate.UserRole}).One(&b)
	fmt.Println(err)
	if b != nil {
		return errors.New("This Candidate already applied for this Batch")
	}

	return rs.DB().C(models.CollectionBatch).Update(bson.M{"batchId": batchID}, bson.M{"$addToSet": bson.M{"candidates": candidate}})
}

// GetCandidatesBasedOnStatus :
func (dao *BatchDAO) GetCandidatesBasedOnStatus(rs app.RequestScope, batchID int64, status string) ([]bson.M, error) {
	result := []bson.M{}
	error := rs.DB().C(models.CollectionBatch).Pipe([]bson.M{bson.M{
		"$match": bson.M{"batchId": batchID},
	},
		bson.M{
			"$unwind": "$candidates",
		},
		bson.M{
			"$match": bson.M{"candidates.status": status},
		},
		bson.M{
			"$group": bson.M{
				"_id":        "$batchId",
				"candidates": bson.M{"$addToSet": "$candidates"},
			},
		}}).All(&result)
	return result, error
}

// GetAppliedCandidates :
// func (dao *BatchDAO) GetAppliedCandidates(rs app.RequestScope, batchID string) (*models.Batch, error) {
// 	var b models.Batch
// 	err := rs.DB().C(models.CollectionBatch).Find(bson.M{"batchId": batchID, "candidates.status": "Applied"}).Select(bson.M{"batchId": 1, "candidates": 1}).One(&b)
// 	return &b, err
// }

// GetBatchByID :
func (dao *BatchDAO) GetBatchByID(rs app.RequestScope, batchID int64, project bson.M) (b *models.Batch, err error) {

	return
}

// GetBatch :
func (dao *BatchDAO) GetBatch(rs app.RequestScope, query bson.M, project bson.M) (b *models.Batch, err error) {
	err = rs.DB().C(models.CollectionBatch).Find(query).Select(project).One(&b)
	return
}

// GetBatches : Upcoming Batches
func (dao *BatchDAO) GetBatches(rs app.RequestScope, query, project bson.M) (b []*models.Batch, batchcount int, err error) {
	pageNo, _ := strconv.Atoi(rs.GetQueries().Get("pageNo"))
	itemsperPage, _ := strconv.Atoi(rs.GetQueries().Get("itemsPerPage"))
	skipValue := (pageNo - 1) * itemsperPage
	batchcount, err = rs.DB().C(models.CollectionBatch).Find(query).Count()
	err = rs.DB().C(models.CollectionBatch).Find(query).Skip(skipValue).Limit(itemsperPage).All(&b)
	return
}

// UpdateBatchByID :
func (dao *BatchDAO) UpdateBatchByID(rs app.RequestScope, batchID int64, update interface{}) error {
	return rs.DB().C(models.CollectionBatch).Update(bson.M{"batchId": batchID}, bson.M{"$set": update})
}

// UpdateBatchWithFindQuery :
func (dao *BatchDAO) UpdateBatchWithFindQuery(rs app.RequestScope, query, update interface{}) error {
	return rs.DB().C(models.CollectionBatch).Update(query, update)
}

// FindOneBatch ...
func (dao *BatchDAO) FindOneBatch(rs app.RequestScope) (b *models.Batch, err error) {
	batchID, _ := strconv.Atoi(rs.GetParams()["batchId"])
	err = rs.DB().C(models.CollectionBatch).Find(bson.M{"batchId": int64(batchID)}).One(&b)
	return
}

func (dao *BatchDAO) FindBatchTypes(rs app.RequestScope) (b []*models.BatchType, err error) {
	err = rs.DB().C(models.CollectionBatchType).Find(nil).All(&b)
	return
}
