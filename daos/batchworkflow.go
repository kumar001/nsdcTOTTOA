package daos

import (
	"nsdctottoaservice/app"
	"nsdctottoaservice/constant"
	"nsdctottoaservice/models"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"
)


type IbatchWorkflowDAO interface {
	FindPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) (bwdata []bson.M, pendingBatchesCount int, err error)
	Post(app.RequestScope, *models.BatchWorkflow) error
	FindBatchworkflow(rs app.RequestScope, userDetails map[string]string) ([]*models.BatchWorkflow, error)
	CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) error

	GetOne(rs app.RequestScope, query, project bson.M) (bw *models.BatchWorkflow, err error)
	Update(rs app.RequestScope, query bson.M, bw *models.BatchWorkflow) error
	FindPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error)
}

// BatchWorkflowDAO :
type BatchWorkflowDAO struct {
}

// GetBatchWorkflowDAO :
func GetBatchWorkflowDAO() *BatchWorkflowDAO {
	return &BatchWorkflowDAO{}
}

// Post : Creates BatchWorkflow
func (bwd *BatchWorkflowDAO) Post(rs app.RequestScope, workflow *models.BatchWorkflow) error {
	workflow.CreatedOn = time.Now()
	return rs.DB().C(models.CollectionBatchWorkflow).Insert(workflow)

}

// GetOne :
func (bwd *BatchWorkflowDAO) GetOne(rs app.RequestScope, query, project bson.M) (bw *models.BatchWorkflow, err error) {
	err = rs.DB().C(models.CollectionBatchWorkflow).Find(query).Select(project).One(&bw)
	return
}

// GetMany :
func (bwd *BatchWorkflowDAO) GetMany(rs app.RequestScope, query, project bson.M) (bws []*models.BatchWorkflow, err error) {
	err = rs.DB().C(models.CollectionBatchWorkflow).Find(query).Select(project).All(&bws)
	return
}

// Update :
func (bwd *BatchWorkflowDAO) Update(rs app.RequestScope, query bson.M, bw *models.BatchWorkflow) error {
	return rs.DB().C(models.CollectionBatchWorkflow).Update(query, bson.M{"$set": bw})
}

// FindBatchworkflow :
func (bwd *BatchWorkflowDAO) FindBatchworkflow(rs app.RequestScope, userDetails map[string]string) (bws []*models.BatchWorkflow, err error) {
	findQuery := bson.M{"toUserRole": userDetails["role"], "userName": userDetails["userName"]}
	err = rs.DB().C(models.CollectionBatchWorkflow).Find(findQuery).All(&bws)
	return bws, err
}

// CancelPendingWorkflow :
func (bwd *BatchWorkflowDAO) CancelPendingWorkflow(rs app.RequestScope, batchID int64, qpCode, toUserRole, userName string) error {
	q := bson.M{}
	u := bson.M{"status": constant.CANCELLEDBYSSC, "actionTakenOn": time.Now()}
	if qpCode == "" {
		// TC
		q = bson.M{"status": constant.NEW, "batchId": batchID, "toUserRole": toUserRole, "userName": userName}
	} else {
		// MT, AA or A
		q = bson.M{"status": constant.NEW, "batchId": batchID, "toUserRole": toUserRole, "qpCode": qpCode, "userName": userName}
	}

	_, err := rs.DB().C(models.CollectionBatchWorkflow).UpdateAll(q, bson.M{"$set": u})
	return err
}

// func (bwd *BatchWorkflowDAO) FindBatchworkflow(rs app.RequestScope, userDetails map[string]string) (bws []*models.BatchWorkflow, count int, err error) {
// 	findQuery := bson.M{"toUserRole": userDetails["role"], "userName": userDetails["userName"]}
// 	count, _ = rs.DB().C(models.CollectionBatchWorkflow).Find(findQuery).Count()
// 	err = rs.DB().C(models.CollectionBatchWorkflow).Find(findQuery).All(&bws)
// 	return bws, count, err
// }
func (bwd *BatchWorkflowDAO) FindPendingBatches(rs app.RequestScope, tokenValue *models.JWTClaims, paginate map[string]string) (bwdata []bson.M, pendingBatchesCount int, err error) {

	col := rs.DB().C(models.CollectionBatchWorkflow)
	status := rs.GetParams()["status"]
	query := []bson.M{
		{"$match": bson.M{
			"status":     status,
			"toUserRole": tokenValue.Role,
			"userName":   tokenValue.UserName,
		}},
		{"$lookup": bson.M{
			"from":         "batch",
			"localField":   "batchId",
			"foreignField": "batchId",
			"as":           "BatchDetails",
		}}}

	pipeCount := col.Pipe(query)
	err = pipeCount.All(&bwdata)
	pendingBatchesCount = len(bwdata)

	pageNo, _ := strconv.Atoi(paginate["pageNo"])
	itemsperPage, _ := strconv.Atoi(paginate["itemsPerPage"])
	skipValue := (pageNo - 1) * itemsperPage

	query = append(query, bson.M{
		"$skip": skipValue,
	}, bson.M{
		"$limit": itemsperPage,
	})
	pipe := col.Pipe(query)
	err = pipe.All(&bwdata)

	return bwdata, pendingBatchesCount, err
}

func (bwd *BatchWorkflowDAO) FindPendingBatch(rs app.RequestScope) (bwdata []bson.M, err error) {

	col := rs.DB().C(models.CollectionBatchWorkflow)
	reqID, _ := strconv.Atoi(rs.GetParams()["id"])
	query := []bson.M{
		{"$match": bson.M{
			"reqId": int64(reqID),
		}},
		{"$lookup": bson.M{
			"from":         "batch",
			"localField":   "batchId",
			"foreignField": "batchId",
			"as":           "BatchDetails",
		}}}

	pipe := col.Pipe(query)
	err = pipe.All(&bwdata)
	return bwdata, err
}
