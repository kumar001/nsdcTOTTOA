package daos

import (
	"encoding/json"
	"fmt"
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"

	"gopkg.in/mgo.v2/bson"
)

// AdminDAO :
type AdminDAO struct {
}

// NewAdminDao : create instance of admindao
func NewAdminDao() *AdminDAO {
	return &AdminDAO{}
}

func (dao *AdminDAO) GetAdminConfiguration(rs app.RequestScope, projectedFields []string) (models.TOTConfiguration, error) {

	var result models.TOTConfiguration
	mongoProjectedFields := "{"
	for i := 0; i < len(projectedFields); i++ {
		mongoProjectedFields += `"` + projectedFields[i] + `"` + ":" + "1" + ","
	}
	mongoProjectedFields += `"_id":0}`
	var m map[string]interface{}
	err := json.Unmarshal([]byte(mongoProjectedFields), &m)
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	find := bson.M{}
	err = rs.DB().C(models.TOTConfigurationCollection).Find(find).Select(m).One(&result)
	return result, err
}

func (dao *AdminDAO) GetAdminConfigurationToa(rs app.RequestScope, projectedFields []string) (models.TOAConfiguration, error) {
	var result models.TOAConfiguration
	mongoProjectedFields := "{"
	for i := 0; i < len(projectedFields); i++ {
		mongoProjectedFields += `"` + projectedFields[i] + `"` + ":" + "1" + ","
	}
	mongoProjectedFields += `"_id":0}`
	var m map[string]interface{}
	err := json.Unmarshal([]byte(mongoProjectedFields), &m)
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	find := bson.M{}
	err = rs.DB().C(models.TOAConfigurationCollection).Find(find).Select(m).One(&result)
	return result, err
}
