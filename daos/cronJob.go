package daos

import (
	"errors"
	"nsdctottoaservice/app"
	"nsdctottoaservice/models"

	"gopkg.in/mgo.v2/bson"
)

type CronJobDAO struct {
}

func NewcronJobDao() *CronJobDAO {
	return &CronJobDAO{}
}

// UpDateBatches ... Update Many Batch
func (dao *CronJobDAO) UpDateBatches(rs app.RequestScope, findQuery, updateQuery bson.M) error {
	info, err := rs.DB().C(models.CollectionBatch).UpdateAll(findQuery, updateQuery)
	if info.Updated == 0 || info.Matched == 0 {
		return errors.New("You Don't Have a Batch to Rejects")
	}
	return err
}

// UpdateBatchworkFlows ... Update Many BatchworkFlow
func (dao *CronJobDAO) UpDateBatchworkFlows(rs app.RequestScope, findQuery, updateQuery bson.M) error {
	info, err := rs.DB().C(models.CollectionBatchWorkflow).UpdateAll(findQuery, updateQuery)
	if info.Updated == 0 || info.Matched == 0 {
		return errors.New("You Don't Have a BatchWorkflow to Rejects")
	}
	return err
}

// GetBatchIdsFromBwflow ...
func (dao *CronJobDAO) GetBatchIdsFromBwflow(rs app.RequestScope, findQuery bson.M) (batchIds []int64, err error) {

	var batchWorkFlowDetails []*models.BatchWorkflow
	err = rs.DB().C(models.CollectionBatchWorkflow).Find(findQuery).All(&batchWorkFlowDetails)
	for _, bw := range batchWorkFlowDetails {
		// log.Println("------bw", bw)
		batchIds = append(batchIds, bw.BatchID)
	}
	return removeDuplicates(batchIds), nil
}

func removeDuplicates(elements []int64) []int64 {
	// Use map to record duplicates as we find them.
	encountered := map[int64]bool{}
	result := []int64{}
	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}
